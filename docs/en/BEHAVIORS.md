# Behaviors

Behaviors are a design pattern used to implement any kind of user interaction with the app, especially the canvas.

## Examples

The behavior `BSelectOnClick` listens to DOM `'click'` click events, and outputs an `'input'` event, which is used by Vue for upwards data transfer.

The behavior `BResizeOnArrowDrag` will listen to DOM `'mousedown'`, `'mousemove'` and `'mouseup'` to detect a drag on one of the selection arrows. As the drag progresses, it will call `updateLabel` to modify a label duration.

## Data exchange

Behaviors explicitely state a typed list of Vue props. The most common one is `register`, which allows them to register event listeners that are freed automatically when they are removed.

The second most common is `actions`, the main API of the App. Defined in [`AnalysisActions`](../../src/logic/AnalysisActions.ts), it allows to modify the current analysis.

The component [`CanvasScroller`](../../src/components/canvas-addons/CanvasScroller.vue) also provides a way for behaviors to control the App scrolling, passing its own Api to its slots.

They have access to canvas and document events through the canvas, which wraps them. `BResizeOnArrowDrag` listens to `'right-arrow-mousedown'`, `'left-arrow-mousedown'` and `'document-mouseleave'` for example.

They may listen to specialized events, such as `'label-shift-dblclick'`.

They can, if necessary, send custom events to another behavior. In that case, the receiving behavior must explicitely state what event they can listen to, and what parameters it needs. The sending behavior must handle the lack of receiving behavior gracefully. For example, `'BCreateOnArrowDrag'` creates a label, but then simply asks `'BResizeOnArrowDrag'` to resize it with the current user drag.

Behaviors inherit from `LabelBehavior`, which provides basic shared elements to these behaviors.

Some behaviors draw basic SVG components, using the standard Vue template.

## Philosophy

Any user interaction on the canvas, especially if is subject to change depending on platform should be a behavior.

Since they state exactly all they info they need using props, and all the info they output (through events or API calls), they are easy to test. See the tests for [`BDeleteOnKey`](../../tests/unit/behaviors/shared/BDeleteOnKey.spec.ts)

## Dependencies

Here's a dependency graph of behaviors, as a visual aid

<img src="../graphs/behaviors.svg">