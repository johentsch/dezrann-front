module.exports = {
  root: true,
  env: {
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
  },
  extends: [
    'plugin:vue/recommended',
    'plugin:vuetify/recommended',
    '@vue/airbnb',
    '@vue/typescript/recommended',
  ],
  rules: {
    // Vue additional checks
    'vue/component-name-in-template-casing': ['error', 'kebab-case'],
    'vue/custom-event-name-casing': ['error', 'kebab-case'],
    'vue/html-comment-indent': 'error',
    'vue/match-component-file-name': ['error', {
      extensions: ['.vue', 'vue', 'vue.ts', 'jsx'],
      shouldMatchCase: true,
    }],
    'vue/next-tick-style': 'error',
    'vue/no-boolean-default': 'error',
    'vue/no-duplicate-attr-inheritance': 'error',
    'vue/no-potential-component-option-typo': 'error',
    'vue/no-reserved-component-names': 'error',
    'vue/no-template-target-blank': 'error',
    'vue/no-useless-mustaches': 'error',
    'vue/no-useless-v-bind': 'error',
    'vue/v-for-delimiter-style': 'error',
    'vue/valid-next-tick': 'error',
    'vue/max-attributes-per-line': ['error', {
      singleline: 3,
      multiline: 2,
    }],
    'vue/block-lang': ['error', {
      script: { lang: 'ts' },
      style: { lang: 'scss' },
      i18n: { lang: 'yaml' },
    }],
    'vue/use-v-on-exact': ['warn'],
    'vue/no-unused-properties': 'warn',
    'vue/prefer-separate-static-class': 'warn',
    // Removed because does not detect vue-class-component registration
    // 'vue/no-unregistered-components': ['error', {
    //   ignorePatterns: ['^v-', 'dialog-card'],
    // }],
    yoda: ['error', 'never', { exceptRange: true }],
    'no-negated-condition': 'error',
    '@typescript-eslint/consistent-type-imports': ['warn', {
      prefer: 'type-imports',
      disallowTypeAnnotations: true,
    }],

    // Allow
    'class-methods-use-this': 'off',
    'vue/new-line-between-multi-line-property': 'off',

    // Formatting

    // Ignore indention after decorator
    // typescript eslint will not fix broken indentation
    '@typescript-eslint/indent': ['error', 2, {
      ignoredNodes: ['*[decorators.length > 0] > Identifier'],
      SwitchCase: 1,
    }],
    indent: ['off'],

    '@typescript-eslint/quotes': ['error', 'single'],

    // Disable JS no-shadowing, but enable typescript version
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],

    // Disable JS semi, but enable typescript version
    semi: 'off',
    '@typescript-eslint/semi': ['error'],

    // Disable boundary types for vue functions
    // Avoid `:void` on every Vue callbacks
    '@typescript-eslint/explicit-module-boundary-types': 'off',

    // Disable forced default export
    'import/prefer-default-export': 'off',

    'max-classes-per-file': 'off',

    '@typescript-eslint/no-empty-function': 'off',

    '@typescript-eslint/no-unused-vars': [
      'warn',
      {
        varsIgnorePattern: '^_$',
        argsIgnorePattern: '^_$',
      },
    ],
  },
  overrides: [
    {
      // enable the rule specifically for TypeScript files
      files: ['*.ts', '*.tsx'],
      rules: {
        '@typescript-eslint/explicit-module-boundary-types': ['error'],
      },
    },
    {
      files: [
        '**/__tests__/*.ts',
        '**/tests/unit/**/*.spec.ts',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
