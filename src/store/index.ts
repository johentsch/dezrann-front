import type { StoreOptions } from 'vuex';
import { module } from './default.module';
import type { DefaultStoreState } from './default.state';

export const storeOpts: StoreOptions<DefaultStoreState> = {
  ...module,
  modules: {},
};
