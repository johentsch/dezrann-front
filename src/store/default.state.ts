import type { Piece } from '@/data';
import PieceParser from '@/formats/PieceParser';

export interface DefaultStoreState {
  primaryColor: string;
  secondaryColor: string;
  corpus: Piece[];
  corpusId: string | null;
  indexCurrentPiece: number;
  allowedToPlay: boolean;
}

export const state = (): DefaultStoreState => ({
  primaryColor: 'primary',
  secondaryColor: 'secondary',
  corpus: [PieceParser.fromFormat('', false, {
    id: '',
    title: '...',
    opus: {},
    sources: {},
  })],
  corpusId: null,
  indexCurrentPiece: 0,
  allowedToPlay: false,
});
