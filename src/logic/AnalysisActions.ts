import type {
  Label,
  LabelId,
  LabelKey,
  LabelPredicate,
  LabelTemplate,
  StaffId,
  Duration,
  Onset,
  Staff,
} from '@/data';
import type SharedActions from './SharedActions';
import type { LabelFilterType, TypeFilter } from './LabelFilters';
import type LabelFilters from './LabelFilters';

type LabelRef = Readonly<Label>;

export default interface AnalysisActions extends SharedActions {
  deleteId(id: LabelId): void;
  select(id: LabelId | null, requestTarget?:boolean): void;
  updateLabel<K extends LabelKey, T extends Label[K]>(id: LabelId, key: K, value: T): void;
  createLabel(): LabelId;
  setTemplate(template: LabelTemplate): void;

  updateSearchString(searchString: string): void;

  getSelected(): LabelRef | null;
  getPrettySelected(): string | null;
  getTemplate(): LabelTemplate;
  findFreeStaff(onset: Onset, duration: Duration, staves: Staff[], hint?: StaffId): StaffId;
  getSortedLabels(): LabelRef[];
  updateTypesFilter(filters: TypeFilter): void;
  updateLayersFilter(filters: TypeFilter): void;
  getFilterValueFor(filter: string, type: LabelFilterType): boolean;
  getLabel(labelId: LabelId): LabelRef | null;
  getPrev(labelId: LabelId | null, predicate?: LabelPredicate): LabelRef | null;
  getNext(labelId: LabelId | null, predicate?: LabelPredicate): LabelRef | null;

  readonly editCount: number;
  readonly tagEditCount: number;
  readonly selectCount: number;
  readonly targetCount: number;
  readonly selectedId: LabelId | null;
  readonly template: LabelTemplate | null;
  readonly labelFilters: LabelFilters | null;
}
