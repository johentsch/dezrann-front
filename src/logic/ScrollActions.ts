import type { Pixel } from '@/data';

export type ScrollTo = (p: Pixel, smooth?: boolean) => void;
export type ScrollToRange = (p: Pixel, w: Pixel, smooth?: boolean) => void;

export default interface ScrollActions {
  goTo: ScrollTo;
  goToRange: ScrollToRange;
}
