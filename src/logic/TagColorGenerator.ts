import type { CssGenerator } from './CSSGenerator';
import { makeCssIdentifier } from './CSSGenerator';

interface Range {
  start: number;
  end: number;
}

const PHI = 1.618033988749895;
const HUE_RANGE = { start: 0, end: 360 };
const SAT_RANGE = { start: 100, end: 50 };
const LIT_RANGE = { start: 50, end: 40 };

function mapNumber(range: Range, ratio: number): number {
  return ((ratio % 1) * (range.end - range.start)) + range.start;
}

function generateColor(i: number) {
  const hue = mapNumber(HUE_RANGE, i * PHI);
  const sat = mapNumber(SAT_RANGE, i * PHI * PHI);
  const lit = mapNumber(LIT_RANGE, i * PHI * PHI * PHI);
  return `hsl(${hue}, ${sat}%, ${lit}%)`;
}

export class TagColorGenerator implements CssGenerator {
  /**
   * A marker to modify on update
   * Will trigger a re-render
   * */
  public modificationCounter = 0;

  // Array of all tags
  // Position in the array is used to find the color
  // When tags are removed, they are replaced by null
  // So the color can be used again
  knownTags: (string | null)[] = [];

  updateUsedColors(usedTags: string[]): void {
    const usedTagsSet = new Set(usedTags.map(makeCssIdentifier));

    // Replace all unused tags in the array by null
    this.knownTags.forEach((t, i) => {
      if (t !== null && !usedTagsSet.has(t)) {
        this.knownTags[i] = null;
      }
    });

    // Create a set from known tags
    const knownTagsSet = new Set(this.knownTags);

    // Find every new tags
    const newTags = [...usedTagsSet].filter((t) => !knownTagsSet.has(t));

    let knownTagIndex = 0;
    // Iterate on new tags
    newTags.forEach((t) => {
      // Iterate on known tags until room is found
      while (this.knownTags[knownTagIndex]) knownTagIndex += 1;

      // Add the new tag to the known tags
      this.knownTags[knownTagIndex] = t;
    });

    // If new tags were added, re-render
    if (newTags.length) this.modificationCounter += 1;
  }

  renderCss(): string {
    return this.knownTags
      .filter((t) => t)
      .map((t, i) => `
      .tag-${makeCssIdentifier(t as string)} {
        --tag-color: ${generateColor(i)};
      }`)
      .join('\n');
  }
}
