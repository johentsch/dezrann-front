import type { LabelTemplate, FilterGroup, Filter } from '@/data';
import type { CssGenerator } from './CSSGenerator';

/**
 * Describes a group of filters
 *
 * Can match labels to rules
 *
 * Can generate css rules
 */
export class FilterStyleGenerator implements CssGenerator {
  private filters: FilterGroup;

  constructor(filters: FilterGroup) {
    this.filters = filters;
  }

  modificationCounter = 0;

  /**
   * Transform a string into a valid css identifier
   * Collisions are possible if special characters are used
   */
  static makeCssIdentifier(value: string): string {
    // https://stackoverflow.com/questions/10619126/make-sure-string-is-a-valid-css-id-name
    // We must be careful not to replace into an invalid string,
    // thus adding 'a' in some cases and doing a second replace.
    return value?.replace(/(^-\d-|^\d|^-\d|^--)/, 'a$1').replace(/[\W]/g, '-') || '';
  }

  /**
   * @returns a css file string to add to the document
   */
  renderCss(): string {
    return this.filters.filters.map(FilterStyleGenerator.typeToCss).join('');
  }

  /**
   * @param type a valid filter
   * @returns a set of css classes representing the associated style
   */
  private static typeToCss(type: Filter): string {
    let rules = '';

    const cssId = FilterStyleGenerator.getCssSelector(type.type, type.tag, type.layers);

    // If the type uses automatic colors
    // Use the tag color instead
    if (type.color === 'auto') {
      rules += `${cssId} { --label-color: var(--tag-color, white) }\n`;
    } else if (type.color) {
      rules += `${cssId} { --label-color: ${type.color} }\n`;
    }
    // Generate the font styling from the filter
    if (type.fontWeight) {
      rules += `.label-text${cssId} { font-weight: ${type.fontWeight} }\n`;
    }
    if (type.fontSize) {
      rules += `.label-text${cssId} { font-size: ${type.fontSize} }\n`;
    }
    if (type.fontStyle) {
      rules += `.label-text${cssId} { font-style: ${type.fontStyle} }\n`;
    }
    return rules;
  }

  /**
   * @param label a valid Label
   * @returns all the css classes to associate with the label for proper display
   */
  static getCssClassNames(label: LabelTemplate): string {
    let classes = `type-${FilterStyleGenerator.makeCssIdentifier(label.type)} `;
    classes += `tag-${FilterStyleGenerator.makeCssIdentifier(label.tag.trim())} `;
    classes += label.layers?.map((l) => `layer-${l}`).join(' ') ?? '';

    return classes;
  }

  /**
   * @param type tje type of a Label
   * @param layers the layers of a Label
   * @returns a selector that will match any Label with this type and all these layers
   */
  static getCssSelector(type: string | null, tag: string | null, layers: string[] = []): string {
    let classes = type ? `.type-${FilterStyleGenerator.makeCssIdentifier(type)}` : '';
    classes += tag ? `.tag-${FilterStyleGenerator.makeCssIdentifier(tag.trim())}` : '';
    classes += layers.map((l) => `.layer-${l}`).join('');

    return classes;
  }
}
