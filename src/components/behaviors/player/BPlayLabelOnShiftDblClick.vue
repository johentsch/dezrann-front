<template>
  <rect
    v-if="state && state.pixelInterval"
    :x="state.pixelInterval.start"
    :width="state.pixelInterval.width"
    height="3px"
    opacity="0.5"
    @click="state = null"
  />
</template>
<script lang="ts">
import {
  Component, Prop, PropSync, Watch,
} from 'vue-property-decorator';
import type {
  Duration, Label, Onset, Pixel, Range, Unit,
} from '@/data';
import LabelBehavior from '../LabelBehavior.vue';

/**
 * The state of the behavior, representing a loop
 */
interface State {
  /** the start of the loop range */
  readonly onset: Onset
  /** the end of the loop range */
  readonly offset: Onset
  /** the pixel interval to display, undefined if no unit */
  readonly pixelInterval?: Range<Pixel>
  /** if the label should loop once finished */
  readonly looping: boolean;
  /** if the currentTime has not yet moved to the required position after update event */
  seeking: boolean;
}

/**
 * Maximum error in the range, in musical time for convenience
 * some error is expected, due to
 * * Units being only of limited precision
 * * A minimum amount of time passing between two events
 */
const EPSILON = 1 as Duration;

function checkTime(currentTime: Onset, target: Onset): boolean {
  return Math.abs(currentTime - target) < EPSILON;
}

/**
 * Create a playback loop on a label, that plays once or more
 * Due to the inherent asynchronicity of players, and delays in applying currentTime,
 * this is inherently stateful and prone to problems and race conditions
 * large margins are used to try and account for this
 *
 * possible future issues:
 * * loops stops on its own when reaching the end: false positive on the seek detection
 * * loop displays stays on but does nothing: behavior is stuck on seek mode
 */
@Component
export default class BPlayLabelOnShiftClick extends LabelBehavior {
  /**
   * The unit used to display the range of the loop
   */
  @Prop({ required: false })
  pixelUnit?: Unit<Pixel>;

  /**
   * The cursor position, used for looping
   */
  @PropSync('musicalTime', { required: true })
  musicalTimeSync!: Onset | null;

  /**
   * If the player is playing
   */
  @PropSync('playing', { required: true })
  playingSync!: boolean;

  /**
   * Additional musical time after onset
   */
  @Prop({ default: 0 })
  offsetMargin!: Duration;

  state: State | null = null;

  @Watch('musicalTimeSync')
  onTimeChange(newTime: Onset | null, oldTime: Onset | null) {
    // Check state, clear if invalid
    if (this.state === null || newTime === null || oldTime === null) {
      this.state = null;
      return;
    }

    // If currently seeking
    if (this.state.seeking) {
      // Check if seeking is complete, store the first timestamp
      if (checkTime(newTime, this.state.onset)) this.state.seeking = false;
      // Wait for next update
      return;
    }

    // If this behavior stops looping for no reason
    // it is likely due to 'bounce' in FilePlayer or YoutubePlayer time events
    // simply replace checkTime by checking if newTime is between onset and offset

    // If user has paused or moved cursor away from allowed range, stop the loop
    if (!this.playingSync || !checkTime(newTime, oldTime)) {
      this.state = null;
      return;
    }

    // If the loop has reached its end, reset the loop
    if (newTime >= this.state.offset) {
      // Restart
      if (this.state.looping) {
        // Set as seeking
        this.state.seeking = true;
        // Send update event
        this.musicalTimeSync = this.state.onset;
      // Pause
      } else {
        // Delete state
        this.state = null;
        // pause
        this.playingSync = false;
      }
    }
  }

  startLoop(label: Label, looping: boolean) {
    // If label is staff, ignore event
    if (!label.duration) return;

    // Try to get display interval
    const pixelInterval = this.pixelUnit?.fromInterval(label.onset, label.duration);

    // Calculate offset, with margin to hear last note
    const offset = (label.onset + label.duration + this.offsetMargin) as Onset;
    // Init state
    this.state = {
      onset: label.onset,
      offset,
      seeking: true,
      pixelInterval,
      looping,
    };
    // Send update:musicalTime event
    this.musicalTimeSync = label.onset;
    // Send update:playing event
    this.playingSync = true;
  }

  mounted() {
    // Play label on shift dbl click
    this.register('label-shift-dblclick', (e, label) => this.startLoop(label, false));
    // Loop label on ctrl shift dbl-click
    this.register('label-ctrl-shift-dblclick', (e, label) => this.startLoop(label, true));
  }
}

</script>
