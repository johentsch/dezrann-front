import type { CanvasEventSource, Registerer } from '../canvas/canvas-events';

interface IListenerAutoCleaner {

  /**
   * Register a new event listener on the canvas
   * @param eventName the name of the event, with possible modifiers
   * @param callback the callback
   */
  register: Registerer;

  /**
   * Unregister all event listeners added to this
   */
  clear(): void;
}

class ListenerAutoCleanerImpl implements IListenerAutoCleaner {
  /**
   * Create a new listener auto-cleaning list
   * @param canvas the canvas event API
   */
  constructor(canvas: CanvasEventSource) {
    this.canvas = canvas;
  }

  /**
   * The canvas event API
   */
  private canvas: CanvasEventSource;

  register(event: unknown, callback: unknown): void {
    this.listeners.push([event, callback]);

    // Pass the event to the canvas
    // Type checking is done by the IListenerAutoCleaner
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this.canvas.on(event as any, callback as any);
  }

  private listeners: [unknown, unknown][] = [];

  clear(): void {
    this.listeners.forEach(([event, callback]) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      this.canvas.off(event as any, callback as any);
    });
    this.listeners = [];
  }
}

// Exported class type is modified to ensure type safety
// `register` function is of type `Registerer` instead of `(unknown, unknown) => void`
export const ListenerAutoCleaner = ListenerAutoCleanerImpl;
export type ListenerAutoCleaner = IListenerAutoCleaner;
