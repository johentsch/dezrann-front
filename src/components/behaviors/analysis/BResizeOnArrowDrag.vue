<script lang="ts">
import { Prop, Component } from 'vue-property-decorator';
import type AnalysisActions from '@/logic/AnalysisActions';
import type {
  Label,
  LabelId,
  Unit,
  Duration,
  Pixel,
  Onset,
  Staff,
} from '@/data';
import {
  StaffType,
  STAFF_ID_OFFSET,
} from '@/data';

import type { Image3DPageData } from '@/data/Source';
import PixelUtils from '@/data/PixelUtils';
import type { CanvasMouseEvent, Registerer, Emitter } from '../LabelBehavior.vue';
import LabelBehavior from '../LabelBehavior.vue';
import { roundX, MIN_ONSETS_COVERAGE_RATE } from './utils';
import type { BEdgeDragEmitter } from '../scroll/BScrollOnEdgeDrag.vue';

/**
 * The type of the startResize public event handler
 *
 * Allows any component to ask for a resize
 */
type StartResize = (
  target: LabelId, staff: Staff, draggedX: Pixel, offsetX: Pixel
) => void;

/**
 * Extension of the Registerer type
 *
 * Allows BResizeOnArrowDrag to subscribe to the public event 'start-resize'
 */
type BResizeRegisterer = Registerer & {
  (name: 'start-resize', handler: StartResize): void
};

/**
 * Emitter type, to wrap LabelCanvas.$emit
 *
 * Allows any component to emit 'start-resize' events
 */
export type BResizeEmitter =
  Emitter & ((name: 'start-resize', ...payload: Parameters<StartResize>) => void);

const MAX_HORIZONTAL_DRAG_FOR_BAR = 40;
const MIN_HORIZONTAL_DRAG_FOR_SNAP = 15;

class State {
  /** The label id to commit modifications to */
  target: LabelId;

  /** The difference in mouse X and label moving side on resize start */
  offsetX: Pixel;

  /** The x value of the non-moving side of the label */
  fixedX: Pixel;

  /** The original staff of the label */
  startStaff: Staff;

  /** If the user has dragged the mouse outside the staff */
  hasDraggedVertical: boolean;

  constructor(
    target: LabelId,
    offsetX: Pixel,
    fixedX: Pixel,
    startStaff: Staff,
    hasDraggedVertical: boolean,
  ) {
    this.target = target;
    this.offsetX = offsetX;
    this.fixedX = fixedX;
    this.startStaff = startStaff;
    this.hasDraggedVertical = hasDraggedVertical;
  }

  isGlobalBar(x1: Pixel, x2: Pixel): boolean {
    return this.hasDraggedVertical && x2 - x1 < MAX_HORIZONTAL_DRAG_FOR_BAR;
  }

  isBar(x1: Pixel, x2: Pixel): boolean {
    return this.isGlobalBar(x1, x2) || x2 - x1 < MIN_HORIZONTAL_DRAG_FOR_SNAP;
  }

  calculateSidesPositions(x: number): [Pixel, Pixel, boolean] {
    // Get the new range in pixels of the label
    const [x1, x2] = [this.fixedX, (x + this.offsetX) as Pixel];
    // If the range is in the wrong order, swap them
    return ((x1 <= x2) ? [x1, x2, false] : [x2, x1, true]);
  }
}

/**
 * Resize label on selection arrow drag
 *
 * Uses snapping to match labels to notes
 *
 * Can also receive event 'start-resize' to start resize from another component
 */
@Component
export default class BResizeOnArrowDrag extends LabelBehavior<BEdgeDragEmitter, BResizeRegisterer> {
  @Prop()
  actions!: AnalysisActions;

  @Prop()
  pixelUnit!: Unit<Pixel>;

  @Prop({ default: false })
  snap!: boolean;

  @Prop({ default: false })
  snappableWave!: boolean;

  @Prop()
  pages!: Array<Image3DPageData>;

  state: State | null = null;

  startDrag(
    target: LabelId,
    staff: Staff,
    fixedX: Pixel,
    offsetX: Pixel,
  ): void {
    this.state = new State(
      target,
      offsetX,
      fixedX,
      staff,
      false,
    );
  }

  snapPixelToOnset(pixel: Pixel): Onset {
    // If onset coverage is enough for snapping to onsets
    const snappableWave = this.snappableWave
      && this.pixelUnit.onsetsCoverage() > MIN_ONSETS_COVERAGE_RATE;

    // Snap to onset
    if (snappableWave) {
      const onset = this.pixelUnit.toOnset(pixel, this.snap);
      return Math.round(onset) as Onset;
    }
    // Snap to pixel (avoids bad label pos)
    const roundedX = roundX(pixel);
    return this.pixelUnit.toOnset(roundedX, this.snap);
  }

  drag(event: CanvasMouseEvent): void {
    if (this.state === null) return;

    const [x1, x2, resizingLeft] = this.state.calculateSidesPositions(
      PixelUtils.getMuxPixelFromPosition(this.pages, event.x, event.y),
    );

    let staffId = this.state.startStaff.id || STAFF_ID_OFFSET;
    let duration = 0 as Duration;
    let onset: Onset;
    if (this.state.isBar(x1, x2)) {
      onset = this.pixelUnit
        .toOnset(PixelUtils.getMuxPixelFromPosition(
          this.pages,
          this.state.fixedX,
          event.y,
        ), this.snap);
      if (this.state.isGlobalBar(x1, x2)) staffId = 0;
    } else {
      let offset: Onset;

      // If resizing to the left, only snap onset
      if (resizingLeft) {
        onset = this.snapPixelToOnset(PixelUtils.getMuxPixelFromPosition(
          this.pages,
          x1 as Pixel,
          event.y,
        ));

        //! Remove Snap
        offset = this.snapPixelToOnset(PixelUtils.mux({
          page: PixelUtils.deMux(this.state.fixedX).page,
          row: PixelUtils.deMux(this.state.fixedX).row,
          x: x2 as Pixel,
        }));
      // If resizing to the right, only snap offset
      } else {
        //! Remove Snap
        onset = this.snapPixelToOnset(PixelUtils.mux({
          page: PixelUtils.deMux(this.state.fixedX).page,
          row: PixelUtils.deMux(this.state.fixedX).row,
          x: x1 as Pixel,
        }));
        offset = this.snapPixelToOnset(PixelUtils.getMuxPixelFromPosition(
          this.pages,
          x2 as Pixel,
          event.y,
        ));
      }
      duration = (offset - onset) as Duration;
    }
    this.updateLabel(staffId, onset, duration);

    this.emit('check-edge-drag', event.x);
  }

  updateLabel(staffId: number, onset: Onset, duration: Duration) {
    if (this.state === null) return;
    this.actions.updateLabel(this.state.target, 'staffId', staffId);
    this.actions.updateLabel(this.state.target, 'onset', onset);
    this.actions.updateLabel(this.state.target, 'duration', duration);
  }

  endDrag() {
    this.state = null;
  }

  dragVertical(_: unknown, staff: Staff) {
    if (this.state && staff.type !== StaffType.bar) {
      this.state.hasDraggedVertical = staff.id !== this.state.startStaff.id;
    }
  }

  mounted() {
    // Register the event for CreateOnDrag
    this.register('start-resize', this.startDrag);

    // Register the event for dragging the selection arrows

    this.register('right-arrow-mousedown', ((event: CanvasMouseEvent, label: Label, staff:Staff) => {
      // The current range of the label
      const range = this.pixelUnit.fromInterval(label.onset, label.duration);
      // The offset between the click and the dragged end
      const offset = (PixelUtils.deMux(range.end).x - event.x) as Pixel;
      // Start the drag
      this.startDrag(label.id, staff, range.start, offset);
    }));

    this.register('left-arrow-mousedown', ((event: CanvasMouseEvent, label: Label, staff:Staff) => {
      // The current range of the label
      const range = this.pixelUnit.fromInterval(label.onset, label.duration);
      // The offset between the click and the dragged end
      const offset = (PixelUtils.deMux(range.start).x - event.x) as Pixel;
      // Start the drag
      this.startDrag(label.id, staff, range.end, offset);
    }));

    // Register the drag event, which works anywhere on the canvas
    this.register('all-mousemove', this.drag);
    // Register the staff change events
    // Both are registered as it's possible a label is hiding a staff
    this.register('staff-mousemove', this.dragVertical);
    this.register('label-mousemove', (e, label, staff) => this.dragVertical(e, staff));
    // Register the end drag events, which happen when the mouse is released in or out of the canvas
    this.register('document-mouseup', this.endDrag);
  }
}
</script>
