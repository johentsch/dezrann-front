<script lang="ts">
import {
  Component, Prop, Vue, Watch,
} from 'vue-property-decorator';
import type AnalysisActions from '@/logic/AnalysisActions';
import type { Label, LabelKey } from '@/data';

/**
 * Base class for a label editing field
 *
 * Sets common props
 * * actions: to update the label
 * * label: the label to modify
 *
 * Setup event handling
 * * mounted -> load starting values
 * * input -> try to push changes to the field
 * * external change -> update the field after external changes
 *
 * Validation
 * * Run the parse function to get error messages on input
 *
 * The class is a template to allow for type safety of the conversion functions
 *
 * To use, extend the class, and define the following members
 * * parse: return new values from string input
 * * print: display current value into the field
 * * key: the key of the label property to modify
 *
 * Then update `field` using v-model or manually, and call onInput on user input
 */
@Component
export default class LabelField<K extends LabelKey> extends Vue {
  private render(): null { return null; }

  /**
   * @abstract
   * The key of the label to modify
   */
  protected key!: K;

  /**
   * @abstract
   * @argument val: the value of the field, to convert to T
   * @returns a new value of T for the label
   * @throws an Error containing a human readable message for validation
   */
  protected parse(_: string): Label[K] {
    throw new Error('Not implemented');
  }

  /**
   * @abstract
   * @argument val: a value of T to display
   * @returns the string representation of T
   */
  protected print(_: Label[K]): string {
    throw new Error('Not implemented');
  }

  /**
   * The label to modify
   */
  @Prop({ required: true })
  protected label!: Label | null;

  /**
   * The store actions, to modify the label
   */
  @Prop({ required: true })
  protected actions!: AnalysisActions;

  /**
   * Contains current string value of the field
   */
  protected field = '';

  /**
   * Contains the last parsed value of the field
   * Used to prevent needless update of the field value
   */
  protected lastOk: Label[K] | null = null;

  mounted() {
    this.resetField();

    // Watch the label key for external changes
    // If any happen, reset the field
    // Cannot cause a hanging listener, as the key of a field never changed, even on reuse
    this.$watch(`label.${this.key}`, this.resetField);
  }

  protected resetField() {
    // Initialize the field
    if (this.label === null) {
      this.label = null;
      this.field = '';
    } else {
      this.lastOk = this.label[this.key];
      this.field = this.print(this.lastOk);
    }
  }

  @Watch('label')
  onLabelChange() {
    this.resetField();
  }

  onInput(newFieldValue: string) {
    if (!this.label) return;

    try {
      // Make sure the field is up to date with the input
      this.field = newFieldValue;
      // Get the new value
      const newVal: Label[K] = this.parse(newFieldValue);
      // Get the previous value from the label
      const oldVal: Label[K] = this.label[this.key];

      // If something changed, change the label
      // Store in lastOk to avoid field being updated after this
      if (newVal !== oldVal) {
        this.actions.updateLabel(this.label.id, this.key, newVal);
        this.lastOk = newVal;
      }
    } catch (e) {
      // Ignore errors
      // If the field is not valid, vuetify will have called validate,
      // and an error message will have already appeared
    }
  }

  validate(value: string): true | string {
    try {
      this.parse(value);
      return true;
    } catch (e) {
      return (e as Error).message || `${value} is not valid`;
    }
  }
}
</script>
