export function getMailUrl(subject = '', body = ''): string {
  const encodedSubject = encodeURIComponent(subject);
  const encodedBody = encodeURIComponent(body);
  return `mailto:dezrann@algomus.fr?subject=${encodedSubject}&body=${encodedBody}`;
}

const accountSubject = '[Dezrann] account request';

const accountBody = `Dear Algomus team,

I would like to have an account to store analyses and test the platform for my research/teaching/...

Best regards,

=====================

Chère équipe Algomus,

J'aimerais avoir un compte pour tester la plateforme pour ma recherche / mon enseignement / ...

Cordialement,

`;

export const accountMailUrl = getMailUrl(accountSubject, accountBody);

const contactSubject = '[Dezrann]';

const contactBody = `Dear Algomus team,

I would like to see the following pieces in Dezrann : ...

I would like to have an account to store analyses and test the platform for my research/teaching/...

I have the following suggestion / bug report: ...

Please keep me informed of the development of Dezrann.

Best regards,


=====================

Chère équipe Algomus,

J'aimerais avoir les pièces suivantes sur Dezrann: ...

J'aimerais avoir un compte pour tester la plateforme pour ma recherche / mon enseignement / ...

J'ai une suggestion ou un problème à signaler: ...

Merci de me tenir informé·e des actualités de Dezrann.

Cordialement,

`;

export const contactMailUrl = getMailUrl(contactSubject, contactBody);

const missingPageSubject = '[Dezrann] Missing Page';

const missingPageBody = `Dear Algomus team,

I received a Missing Page error

I was linked to this page from this url: ...

Best regards,`;

export const missingPageMailUrl = getMailUrl(missingPageSubject, missingPageBody);

const missingPieceSubject = '[Dezrann] Missing Piece';

const missingPieceBody = `Dear Algomus team,

I received a Missing Piece error

I was linked to this page from this url: ...

Best regards,`;

export const missingPieceMailUrl = getMailUrl(missingPieceSubject, missingPieceBody);
