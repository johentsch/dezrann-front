import Vue from 'vue';
import type {
  Duration, Onset, Piece, Signature, Unit, SyncIndex, SyncPoint, LabelId, Second,
} from '@/data';
import type { IndexMap } from '@/data/EditableUnit';
import EditableUnit from '@/data/EditableUnit';
import type { SyncLabel } from './SynchronizationActions';
import type SynchronizationActions from './SynchronizationActions';

export default class SynchronizationActionsImpl implements SynchronizationActions {
  /** The unit that will be edited by this set of actions */
  readonly unit: EditableUnit;

  /** The specific table that will be edited */
  readonly points: SyncPoint[];

  /** The signature of the piece, used to display measures as colors */
  readonly signature: Signature;

  constructor(piece: Piece, unit: Unit<number>, tableIndex = 0) {
    this.unit = new EditableUnit(unit.tables, tableIndex);
    this.points = unit.tables[tableIndex];
    this.signature = piece.signature;
    this.tappingIncrement = this.signature.measureIncrement;

    if (!Array.isArray(this.points)) throw new Error('No points for provided points index');

    this.initLabels();
  }

  /** The map from label id to labels, given to the label canvas */
  labels: Record<LabelId, SyncLabel> = {};

  /** The map from sync index to labels */
  /** Used to update indices after editable unit call */
  /** Used to get next and prev */
  /** Could be replaced with SortedLabels with a bit of work */
  sortedLabels: SyncLabel[] = [];

  /** The current selection */
  selectedId: LabelId | null = null;

  // Getters

  /** Return the label for the id if it exists */
  getLabel(id: LabelId): SyncLabel | null {
    return this.labels[id] ?? null;
  }

  /** Return the labels as an array, indexed by SyncIndex */
  getSortedLabels(): SyncLabel[] {
    return this.sortedLabels;
  }

  /** Return a label using an id, throws if id doesn't exist
   * @throws if id is invalid
   */
  getLabelFromId(id: LabelId): SyncLabel {
    if (!this.labels[id]) throw new Error(`Id does not exist ${id}`);
    return this.labels[id];
  }

  /** Returns the label of the following sync point if it exists
   * @throws if id is invalid
   */
  getNext(id: LabelId | null): SyncLabel | null {
    const index = id === null
      ? 0
      : this.getLabelFromId(id).index + 1;
    return this.sortedLabels[index] ?? null;
  }

  /** Returns the label of the previous sync point if it exists
   * @throws if id is invalid
   */
  getPrev(id: LabelId | null): SyncLabel | null {
    const index = id === null
      ? this.sortedLabels.length - 1
      : this.getLabelFromId(id).index - 1;

    return this.sortedLabels[index] ?? null;
  }

  /** Try to get the currently selected label */
  getSelected(): SyncLabel | null {
    if (this.selectedId === null) return null;

    if (this.labels[this.selectedId] === undefined) {
      // eslint-disable-next-line no-console
      console.warn(`Expired selected id: ${this.selectedId}`);
      this.selectedId = null;
      return null;
    }

    return this.labels[this.selectedId];
  }

  getPrettySelected(): null { return null; }

  /** Sets or clear the selection using associated label id
   * @throws if id is invalid
   */
  select(id: LabelId | null, requestTarget = false): void {
    if (id === null) {
      this.selectedId = null;
    } else {
      this.selectedId = this.getLabelFromId(id).id;
    }
    this.selectCount += 1;
    if (this.selectedId !== null && requestTarget) this.targetCount += 1;
  }

  /**
   * Update labels after a return from EditableUnit
   * @param map the index update map, from EditableUnit
   */
  updateLabels(map: IndexMap): void {
    const toDelete: SyncIndex[] = [];

    // For every index that changed position
    map.forEach((newIndex, oldIndex) => {
      // Find the label at the old position
      const label = this.sortedLabels[oldIndex];
      // If label was deleted
      if (newIndex === null) {
        Vue.delete(this.labels, label.id);
        toDelete.push(oldIndex);
      // If label has changed index
      } else {
        this.updateLabel(label, this.points[newIndex], newIndex);
      }
    });

    // Recreate sortedLabels
    // this.sortedLabels = Object.values(this.labels)
    //   .sort((a, b) => a.index - b.index);

    // Update sortedLabels, less safe but faster
    // Remove any removed indices, in reverse order
    toDelete.reverse().forEach((oldIndex) => {
      this.sortedLabels.splice(oldIndex, 1);
    });
    // Make sure it's still sorted
    this.sortedLabels.sort((a, b) => a.index - b.index);

    // Update selection if label was deleted
    if (this.selectedId !== null) {
      if (this.labels[this.selectedId] === undefined) {
        this.select(null);
      }
    }

    // Check everithing is still alright
    this.checkLabelsStillSynced();
    // Update editCount so watchers know something happened
    this.editCount += 1;
  }

  /** Delete a synchronization point and associated label using label id */
  deleteId(id: LabelId): void {
    const label = this.labels[id];
    // Return if ID doesn't exist
    if (!label) return;
    // Return if first or last label
    if (label.index === 0 || label.index === this.sortedLabels.length - 1) return;

    const map = this.unit.deleteSyncPoint(label.index);
    this.updateLabels(map);
    this.editCount += 1;
  }

  /** Create a new sync point, and return the ref */
  /** Since labels are kept sorted, this may update the index of many labels */
  createSyncPoint(p: SyncPoint): SyncLabel {
    // Try to create a new point
    const map = this.unit.createSyncPoint(p);

    // If it succeeds, create a matching label
    const index = this.sortedLabels.length as SyncIndex;
    const label = this.syncPointToLabel(p, index);

    // Add the label to both data structures
    this.sortedLabels.push(label);
    this.labels[label.id] = label;
    // Move that label to the correct index using the map
    this.updateLabels(map);

    // Update edit count
    this.editCount += 1;

    return label;
  }

  /** Update a sync point, this may update the index of a few sync points */
  setSyncPoint(id: LabelId, p: SyncPoint): SyncLabel {
    // Save selection state and point to replace
    const label = this.getLabelFromId(id);
    // Try to update the label
    const map = this.unit.setSyncPoint(label.index, p);
    this.updateLabels(map);
    // WARNING: if updateLabels fails, this may be a reference to a deleted label
    return label;
  }

  // TODO: move actions specific to sync editing (and not just unit editing) to own child class
  // Any action directly using onsets/pixels/seconds is part of that

  /** Create a sync point using interpolation to guess the target value */
  createSyncPointFromOnset(onset: Onset): SyncLabel {
    return this.createSyncPoint(this.unit.getSyncPointFromMusicalTime(onset));
  }

  /** Create a sync point using interpolation to guess the source value */
  createSyncPointFromSecond(timestamp: Second): SyncLabel {
    return this.createSyncPoint(this.unit.getSyncPointFromTarget(timestamp));
  }

  /** Set sync point onset, and interpolate timestamp value from that onset
   * May have unexpected behavior if repeated, as interpolation uses previous value
  */
  setSyncPointFromOnset(id: LabelId, onset: Onset): SyncLabel {
    return this.setSyncPoint(id, this.unit.getSyncPointFromMusicalTime(onset));
  }

  /** Set sync point time, and interpolate onset value from that time
   * May have unexpected behavior if repeated, as interpolation uses previous value
  */
  setSyncPointFromSecond(id: LabelId, timestamp: Second): SyncLabel {
    return this.setSyncPoint(id, this.unit.getSyncPointFromTarget(timestamp));
  }

  /** Try to update only the onset of a sync point
   * Limit the new values to its neighbors and the given margin
  */
  setSyncPointOnset(id: LabelId, onset: Onset, margin = 0): SyncLabel {
    const label = this.getLabelFromId(id);
    const [, oldTarget] = label.point;
    let newSource = this.unit.getSyncPointFromMusicalTime(onset)[0];

    if (margin !== null) newSource = this.boundSource(id, newSource, margin);

    return this.setSyncPoint(id, [newSource, oldTarget]);
  }

  /** Try to update only the target of a sync point
   * Limit the new values to its neighbors and the given margin
  */
  setSyncPointSecond(id: LabelId, timestamp: number, margin = 0): SyncLabel {
    const label = this.getLabelFromId(id);
    const [oldOnset] = label.point;
    let target = this.unit.getSyncPointFromTarget(timestamp)[1];

    if (margin !== null) target = this.boundTarget(id, target, margin);

    return this.setSyncPoint(id, [oldOnset, target]);
  }

  /** Delete the current selection.
   * If selectNext is true, update selection to next element
   * @returns the new selection
  */
  deleteSelected(selectNext = false, requestTarget = false): null | SyncLabel {
    // Delete the current selection

    const label = this.getSelected();

    // If no selection, return
    if (!label) return null;

    // The next label that will be selected
    let next: SyncLabel | null = null;

    // If should selectNext, and there are still points beyond the edges
    if (selectNext && this.points.length > 3) {
      // If current label is second to last, select prev
      if (label.index === this.points.length - 2) next = this.getPrev(label.id);
      // Otherwise, select next
      else next = this.getNext(label.id);
    }

    // Delete selection, and unselect it
    this.deleteId(label.id);

    if (next) this.select(next.id, requestTarget);
    // Return selection or null
    return next;
  }

  /**
   * Given a possible new value of a source,
   * limit it to its neighbors
   * @param index the index of the source to calculate
   * @param source the new value of the source to limit
   * @returns the limited value of the source
   */
  boundSource(id: LabelId, onset: number, margin = 0): Onset {
    const range = this.getAllowedRange(id, true, margin);
    const bounded = Math.max(range[0], Math.min(range[1], onset));
    return bounded as Onset;
  }

  /**
   * Given a possible new value of an onset,
   * limit it to its neighbors
   * @param index the index of the onset to calculate
   * @param onset the new value of the onset to limit
   * @returns the limited value of the onset
   */
  boundTarget(id: LabelId, target: number, margin = 0): number {
    const range = this.getAllowedRange(id, false, margin);
    const bounded = Math.max(range[0], Math.min(range[1], target));
    return bounded as Onset;
  }

  /**
   * Get a sync point max and min values without overlapping another point
   * @param index the index of the sync point
   * @param isOnset if calculating the range of the source or target value of the point
   * @param margin the minimum distance to the next point, unless rangeWidth < margin * 2
   * @returns a tuple representing the range. bounds are infinite if no next/prev point
   */
  getAllowedRange(id: LabelId, isOnset: boolean, margin = 0): [number, number] {
    const prev = this.getPrev(id);
    const next = this.getNext(id);
    const directionIndex = isOnset ? 0 : 1;

    // The allowable range
    const range: [number, number] = [
      prev?.point?.[directionIndex] ?? -Infinity,
      next?.point?.[directionIndex] ?? Infinity,
    ];
    // The allowable range minus two margins
    const rangeMargined: [number, number] = [
      range[0] + margin,
      range[1] - margin,
    ];
    // If margined range is valid, return it
    if (rangeMargined[0] <= rangeMargined[1]) return rangeMargined;

    // Otherwise return 0-sized range in the middle of the bounds
    const avg = (range[0] + range[1]) / 2;
    return [avg, avg];
  }

  /**
   * The musical time increment between two sync points during tapping
   */
  tappingIncrement: Duration;

  getNextTappingOnset(): Onset | null {
    const selected = this.getSelected();
    if (!selected) return null;

    return (selected.onset + this.tappingIncrement) as Onset;
  }

  getSelectedOrFirst(): SyncLabel {
    const selected = this.getSelected() || this.getNext(null);

    if (!selected) throw new Error('Corrupted sync actions, getNext(null) returned null');

    return selected;
  }

  /**
   * Using tappingIncrement, the current selection and a timestamp
   * Try to create a new sync point
   * Select said point if it was created
   * @param second the timestamp of the new sync point
   * @returns the new sync point if it could be created
   */
  createTappingSyncPoint(second: Second): SyncLabel | null {
    // Get the selected label or the first label
    const selected = this.getSelectedOrFirst();

    // Try to create a new label
    try {
      const ref = this.createSyncPoint([
        selected.point[0] + this.tappingIncrement,
        second,
      ]);
      this.select(ref.id);
      return ref;
    } catch {
      return null;
    }
  }

  /**
   * Check that labels and sortedLabels still match points perfectly
   */
  // TODO: remove once ActionsImpl is tested
  checkLabelsStillSynced(): void {
    if (this.points.length !== this.sortedLabels.length) {
      // eslint-disable-next-line no-console
      console.error('Generated labels are not the same length, regenerations');
      this.initLabels();
      return;
    }

    const invalidIndex = this.sortedLabels.findIndex(({ index }, syncIndex) => index !== syncIndex);
    if (invalidIndex >= 0) {
      // eslint-disable-next-line no-console
      console.error('Label index un-synced at', invalidIndex, this.sortedLabels[invalidIndex].index);
      this.initLabels();
      return;
    }

    const invalidOnset = this.sortedLabels
      .findIndex(({ onset, index }) => this.points[index][0] !== onset);

    if (invalidOnset >= 0) {
      // eslint-disable-next-line no-console
      console.error(
        'Label onset un-synced at',
        invalidOnset,
        ': ',
        this.sortedLabels[invalidOnset].onset,
        ' != ',
        this.points[invalidOnset][0],
      );
      this.initLabels();
      return;
    }

    const labels = Object.entries(this.labels);
    if (labels.length !== this.sortedLabels.length) {
      // eslint-disable-next-line no-console
      console.warn('Sorted labels and label map don\'t have the same number of labels');
      this.initLabels();
      return;
    }

    const unsyncedIndex = labels.findIndex(([id, l]) => Number.parseInt(id, 10) !== l.id);
    if (unsyncedIndex >= 0) {
      const [id, label] = labels[unsyncedIndex];
      // eslint-disable-next-line no-console
      console.warn(`Corrupted label map: ${id} !== ${label.id}`);
      this.initLabels();
      return;
    }

    const badIndexIndex = labels.findIndex(([, l]) => (l !== this.sortedLabels[l.index]));
    if (badIndexIndex >= 0) {
      const [, label] = labels[badIndexIndex];
      // eslint-disable-next-line no-console
      console.warn(`Corrupted sorted labels eq: ${label.id} !== ${this.sortedLabels[label.index].id}`);
      this.initLabels();
    }
  }

  /** Init the labels used to display the sync point */
  initLabels(): void {
    // this.sortedLabels
    const labels = this.points.map((p, index) => this.syncPointToLabel(p, index as SyncIndex));

    // Clean label object
    // TODO: check if labels can't just be replaced with new label object
    Object.keys(this.labels).forEach((k) => Vue.delete(this.labels, k));

    // Add back all labels
    labels.forEach((l) => {
      this.labels[l.id] = l;
    });

    // Replace sorted labels array
    this.sortedLabels = labels;
  }

  getTag(onset: Onset): string {
    const measure = this.signature.getMeasureFromOnset(onset);
    return Math.abs(measure).toString();
  }

  /** Update an existing label object with a new point and index */
  updateLabel(label: SyncLabel, point: SyncPoint, index: SyncIndex): void {
    // TODO: for editing positions.json for audio files, calculate correct onset
    const onset = point[0] as Onset;

    Vue.set(label, 'onset', onset);
    Vue.set(label, 'point', point);
    Vue.set(label, 'index', index);
    Vue.set(label, 'tag', this.getTag(onset));
  }

  idCounter = 100;

  /**
   * Create a new label with a new unique id from a sync point
   */
  syncPointToLabel(point: SyncPoint, index: SyncIndex) : SyncLabel {
    this.idCounter += 1;
    // TODO: for editing positions.json for audio files, calculate correct onset
    const onset = point[0] as Onset;

    return {
      onset,
      duration: 0 as Duration,
      durationType: 'point',
      staffId: 0,
      wasStaffLoaded: false,
      type: 'Sync',
      tag: this.getTag(onset),
      comment: '',
      layers: [],
      id: this.idCounter,
      point,
      index,
    };
  }

  editCount = 0;

  selectCount = 0;

  targetCount = 0;
}
