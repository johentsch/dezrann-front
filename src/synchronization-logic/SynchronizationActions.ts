import type {
  Duration, Label, LabelId, Onset, Second, SyncIndex, SyncPoint,
} from '@/data';
import type SharedActions from '@/logic/SharedActions';

export interface SyncLabel extends Label {
  /**
   * The index of the sync point that was used to create the label
   */
  index: SyncIndex;
  /**
   * The sync point that was used to create the label
   */
  point: SyncPoint;
}

export default interface SynchronizationActions extends SharedActions {
  // Sync Point actions
  createSyncPoint(p: SyncPoint): SyncLabel;
  createSyncPointFromOnset(onset: Onset): SyncLabel;
  createSyncPointFromSecond(timestamp: Second): SyncLabel;

  setSyncPoint(id: LabelId, p: SyncPoint): SyncLabel;
  setSyncPointOnset(id: LabelId, onset: Onset, margin?: number): SyncLabel;
  setSyncPointSecond(id: LabelId, timestamp: Second, margin?: number): SyncLabel;
  setSyncPointFromOnset(id: LabelId, onset: Onset): SyncLabel; // Currently unused
  setSyncPointFromSecond(id: LabelId, timestamp: Second): SyncLabel; // Currently unused

  // Label Actions
  getLabel(id: LabelId): SyncLabel | null;
  getSortedLabels(): SyncLabel[];

  select(id: LabelId | null, requestTarget?: boolean): void;
  deleteId(id: LabelId): void;
  getSelected(): SyncLabel | null;
  getPrettySelected(): string | null;
  getNext(id: LabelId | null): SyncLabel | null;
  getPrev(id: LabelId | null): SyncLabel | null;
  deleteSelected(selectNext?: boolean, requestTarget?: boolean): SyncLabel | null;

  // Tapping
  tappingIncrement: Duration;
  getNextTappingOnset(): Onset | null;
  getSelectedOrFirst(): SyncLabel;
  createTappingSyncPoint(second: Second): SyncLabel | null;

  editCount: number;
  selectCount: number;
  targetCount: number;
}
