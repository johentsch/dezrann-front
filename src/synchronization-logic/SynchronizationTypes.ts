import type { TypeArg } from '../data/FilterGroup';
import FilterGroup from '../data/FilterGroup';

/**
 * All the legacy and modern dezrann types
 */
const syncTypesDecl: TypeArg[] = [
  {
    name: 'Sync',
    color: 'auto',
    fontSize: '0px',
  },
];

/**
 * The default filters common to all analysis files
 *
 * A set of useful types with unique colors and css properties
 */
export const syncTypes = FilterGroup.fromTypes(syncTypesDecl);
