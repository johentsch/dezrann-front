
# Localizing Dezrann UI

## Preliminaries

**Understand Dezrann.**
The *target audience* of Dezrann includes both scholars analyzing and/or browsing a piece of corpus, and children/students/people (either from the general public, or from music courses) hearing music and possibly discovering bits of music analysis.

**Do not work alone.** Talk with translators in other languages and developers to see what are the challenges in selecting UI text.

**Be consistent.** Make sure that you use the same terminology, phrasing, and tone throughout the UI.  Begin by discussing the most important terms (some of them are in `elements.json:generic`, but they are used across many items)

|   |   |   |   |   |   |
| - | - | - | - | - | - |
| **en**  | analysis  | layer      | label     | type  | tag    |
| **fr**  | analyse   | couche     | étiquette | type  | tag    |
| **sl**  | analize   | sloj       | etiketo   | vrsta | oznaka |
| **it**  | analisi   | strato     | etichetta | tipo  | tag    |

**Keep the user in mind.** Remember that UI is all about the user. Make sure that your translations are designed to make the user's experience as smooth and intuitive as possible. Use language and formatting that is familiar and easy to understand, and consider the user's cultural background and expectations.

**Prioritize clarity.** Users should be able to quickly and easily understand the meaning of each element in the interface. Avoid using idiomatic expressions or overly complex vocabulary that could be confusing to non-native speakers.
When faced with character limits, prioritize clarity and essential meaning over stylistic flourishes or structural elements.


## How to

The following instructions are if you are a git user. If you are not confortable with git, you can send a mail with the files to someone in the team.

You can directly work with the files in this directory and/or use a tool such as the [i18n Ally extension for VS Code](https://github.com/lokalise/i18n-ally).


**Step 1**
- Decide the main terms and fill the table above
- Start translating by copying the `en/` directory to a new directory with your locale. Note that you can have *empty* or *missing* keys in some files.
- Translate the strings on the right of each line. If you're unsure, leave them empty `""`, we can address them later, but *don't leave unstranslated English string*.
- (i18n Ally: is there a way to empty all the strings at once?)
- Push a branch and create a MR with your files.

**Step 2** (TBD, will come later)
- Check remaining strings
- Translate the *corpora pages* and the *intro/help pages* (TODO: rework them in English first)
- Test your translation with at least one other native speaker

## Further resources

- https://developers.google.com/style/translation
