import { lineIds } from '@/data/Staff';
import type { TypeArg } from './FilterGroup';
import FilterGroup from './FilterGroup';

/**
 * All the legacy and modern dezrann types
 */
const defaultTypesDecl: TypeArg[] = [
  {
    isLegacy: true, name: 'structe', color: '#ffff00', line: lineIds.top1,
  },
  {
    isLegacy: true, name: 'struct=', color: '#ffff00', line: lineIds.top1,
  },
  {
    isLegacy: true, name: 'ped', color: '#335555', line: lineIds.bot1,
  },
  {
    isLegacy: true, name: 'deg', color: '#4400bb', line: lineIds.bot1,
  },
  {
    isLegacy: true, name: 'h', color: '#440000', line: lineIds.bot3,
  },
  {
    isLegacy: true, name: 'seq', color: '#ff8800', line: lineIds.top3,
  },
  {
    isLegacy: true, name: 'seq:all', color: '#ff8800', line: lineIds.top2,
  },
  {
    isLegacy: true, name: 'seqnext', color: '#ee7700', line: lineIds.top3,
  },
  {
    isLegacy: true, name: 'seqnnext', color: '#dd6600', line: lineIds.top3,
  },
  {
    isLegacy: true, name: 'seqnnnext', color: '#cc5500', line: lineIds.top3,
  },
  {
    isLegacy: true, name: 'ext:top', color: '#ff8800', line: lineIds.bot3,
  },
  {
    isLegacy: true, name: 'ext:bot', color: '#ff8800', line: lineIds.bot3,
  },
  {
    isLegacy: true, name: 'pm', color: '#22ff22', line: lineIds.bot3,
  },
  {
    isLegacy: true, name: 'S-inv', color: '#ff4444',
  },
  {
    isLegacy: true, name: 'S-inc', color: '#ffaaaa',
  },
  {
    isLegacy: true, name: 'CS1a', color: '#44ff44',
  },
  {
    isLegacy: true, name: 'CS1b', color: '#22ff22',
  },
  {
    isLegacy: true, name: 'CS1-inc', color: '#aaffaa',
  },
  {
    isLegacy: true, name: 'CS2a', color: '#4444ff',
  },
  {
    isLegacy: true, name: 'CS2b', color: '#2222ff',
  },
  {
    isLegacy: true, name: 'CS2-inc', color: '#aaaaff',
  },
  {
    isLegacy: true, name: 'S2', color: '#aa6600',
  },
  {
    isLegacy: true, name: 'S3', color: '#aa0066',
  },
  { name: 'S', color: '#ff0000' },
  { name: 'CS1', color: '#00ff00' },
  { name: 'CS2', color: '#0000ff' },
  {
    name: 'Pattern', line: lineIds.bot2, color: 'auto',
  },
  {
    name: 'Theme', line: lineIds.bot2, color: '#aa0033',
  },
  {
    name: 'Key', line: lineIds.bot2, color: '#330088',
  },
  {
    isLegacy: true, name: 'Tonality', line: lineIds.bot2, color: '#440088',
  },
  {
    name: 'Modulation', line: lineIds.bot2, color: '#4400ee',
  },
  {
    name: 'Harmony', line: lineIds.bot1, color: '#4400bb',
  },
  {
    name: 'Pedal', line: lineIds.bot1, color: '#335555',
  },
  {
    name: 'Cadence', line: lineIds.all, color: '#ff00ff',
  },
  {
    name: 'Harmonic sequence', line: lineIds.bot3, color: '#ff8800',
  },
  {
    name: 'Texture', line: lineIds.bot3, color: '#22ff22',
  },
  {
    name: 'Structure', line: lineIds.top1, color: '#ffff00',
  },
  {
    name: 'Positive Feedback', color: '#99ff99', fontStyle: 'italic',
  },
  {
    name: 'Feedback', color: '#dddddd', fontStyle: 'italic',
  },
  {
    name: 'Negative Feedback', color: '#ff9999', fontStyle: 'italic',
  },
  {
    name: 'Comment', line: lineIds.bot3, color: '#dddddd', fontStyle: 'italic',
  },
];

/**
 * The default filters common to all analysis files
 *
 * A set of useful types with unique colors and css properties
 */
export const defaultTypes = FilterGroup.fromTypes(defaultTypesDecl);
