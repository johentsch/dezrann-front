/* eslint-disable max-classes-per-file */
// Helper for generating Opaque types.
export type UnitValue<T, K> = T & { __opaque__: K };

// The base Unit used in dezrann
// Can be cast to and from number using 'as'
// This type system completely disappears on compilation
// https://stackoverflow.com/questions/26810574

/**
 * A numerical value in musical time
 */
export type Onset = UnitValue<number, 'Onset'>;

/**
* The difference between two musical time values
*/
export type Duration = UnitValue<number, 'Duration'>;

/**
 * A numerical value in pixels
 */
export type Pixel = UnitValue<number, 'Pixel'>;
/**
 * A numerical value in seconds
 */
export type Second = UnitValue<number, 'Second'>;

export interface Range<T> {
  start: T,
  end: T,
  width: T,
}

export interface Interval {
  onset: Onset;
  offset: Onset;
  duration: Duration;
}

/**
 * An internal type to index the UnitPair tuple
 */
enum Dir {
  ToOnset = 0,
  ToUnit = 1,
}

/**
 * Reexport Dir with a more readable name for use outside
 */
export type BinSearchDirection = Dir;
export const BinSearchDirection = Dir;

/**
 * A pair of equivalent values in two different numerical types
 *
 * Has an untyped default value
 *
 * Internally just a typescript tuple, or fixe-sized array
 */
export type UnitPair<From extends number = number, To extends number = number> =
  Readonly<[From, To]>;

/**
 * A sorted table of equivalent pair, that can be binary searched
 *
 * Has an untyped default value
 */
export type UnitTable<From extends number = number, To extends number = number> =
  UnitPair<From, To>[];

export default class Unit<Target extends number> {
  // Protected constructor, used by public functions
  protected constructor(tables: UnitTable[], scale = 1) {
    this.tables = tables;

    this.scale = scale;
  }

  readonly scale: number;

  readonly tables: UnitTable[];

  /**
   * Convert from musical time to this unit
   * @param onset a numerical value in musical time
   * @returns a numerical value of in unit Target
   */
  fromOnset(onset: Onset): Target {
    return (this.tables.reduce(Unit.binSearchToTarget, onset)
      * this.scale) as Target;
  }

  /**
   * @returns the onsets coverage rate
   */
  onsetsCoverage(): number {
    const lastOnset = this.tables[0].slice(-1)[0][0];
    return this.tables[0].length / lastOnset;
  }

  /**
   * Convert from a musical time interval to an interval in this unit
   * @param onset the start of the interval
   * @param duration the end of the interval
   * @returns an interval in this unit, with a start, end and width
   */
  fromInterval(onset: Onset, duration: Duration): Range<Target> {
    const offset = (onset + duration) as Onset;
    const start = this.fromOnset(onset);
    const end = this.fromOnset(offset);
    return { start, end, width: (end - start) as Target };
  }

  /**
    * Convert from musical time to this unit
    * @param unit a numerical value in unit Target
    * @returns a numerical value in musical type
   */
  toOnset(unit: Target, snap = false): Onset {
    const scaled = unit / this.scale;
    const onset = this.tables.reduceRight(Unit.binSearchToOnset, scaled) as Onset;

    if (snap) return this.snapOnset(onset);
    return onset;
  }

  // Partial conversion functions
  // Used to apply only some of the steps of the conversion table

  // distanceFromOnset correspond to the number of tables from onset
  // For example:
  // converting onset to seconds on a spectrogram table, distanceFromOnset will be 1
  // Because onset is considered index 0, so seconds have a distanceFromOnset of 1
  // If it were 0, fromOnsetToPartial would return the unmodified onset

  protected fromOnsetToPartial(onset: Onset, distanceFromOnset: number): number {
    let val: number = onset;
    for (let i = 0; i < distanceFromOnset; i += 1) {
      val = Unit.binSearchToTarget(val, this.tables[i]);
    }
    return val;
  }

  protected fromPartialToTarget(source: number, distanceFromOnset: number): Target {
    let val: number = source;
    for (let i = distanceFromOnset; i < this.tables.length; i += 1) {
      val = Unit.binSearchToTarget(val, this.tables[i]);
    }
    return (val * this.scale) as Target;
  }

  protected fromTargetToPartial(target: Target, distanceFromOnset: number): number {
    let val: number = target / this.scale;
    for (let i = this.tables.length - 1; i >= distanceFromOnset; i -= 1) {
      val = Unit.binSearchToOnset(val, this.tables[i]);
    }
    return val;
  }

  protected fromPartialToOnset(source: number, distanceFromOnset: number): Onset {
    let val: number = source;
    for (let i = distanceFromOnset - 1; i >= 0; i -= 1) {
      val = Unit.binSearchToOnset(val, this.tables[i]);
    }
    return val as Onset;
  }

  /**
   * Convert from an interval in this unity to a musical time interval
   * @param start the start of the interval
   * @param duration the end of the interval
   * @param snap if the onset interval should be aligned to notes
   * @returns an interval in musical time, with a onset, offset and duration
   */
  toInterval(start: Target, width: Target, snap = false): Interval {
    const end = (start + width) as Target;
    const onset = this.toOnset(start, snap);
    const offset = this.toOnset(end, snap);
    return { onset, offset, duration: (offset - onset) as Duration };
  }

  /**
   * Snaps an onset value to the closest node in the onset unit table
   */
  snapOnset(onset: Onset): Onset {
    return this.boundOnset(onset, true);
  }

  /**
   * Snaps an onset value to the bounds if out of bounds
   * Can also snap to nodes like `snapOnset`
   */
  boundOnset(onset: Onset, snap = false): Onset {
    const table = this.tables[0];
    return Unit.binSearch(onset, table, Dir.ToOnset, Dir.ToOnset, snap) as Onset;
  }

  /**
   * Checks if a unit number will be clamped on conversion to onset
   * @param unit the number to check
   * @returns -1 if before start of score, 0 if inside score, 1 if after score
   */
  isOutOfBounds(unit: Target): number {
    const scaled = unit / this.scale;

    const lastTable = this.tables[this.tables.length - 1];
    const [prevIndex, nextIndex] = Unit.getBounds(scaled, lastTable, Dir.ToUnit);

    if (!Number.isFinite(prevIndex)) return -1;

    if (!Number.isFinite(nextIndex)) return 1;

    return 0;
  }

  /**
   * Snaps an onset value to the closest node in the onset unit table
   */
  snapInterval(onset: Onset, duration: Duration): Interval {
    return this.boundInterval(onset, duration, true);
  }

  /**
   * Snaps an onset value to the bounds of the unit table,
   */
  boundInterval(onset: Onset, duration: Duration, snap = false): Interval {
    const offset = (onset + duration) as Onset;
    const snappedOnset = this.boundOnset(onset, snap);
    const snappedOffset = this.boundOnset(offset, snap);
    return {
      onset: snappedOnset,
      offset: snappedOffset,
      duration: (snappedOffset - snappedOnset) as Duration,
    };
  }

  /**
   * A bin search in a table, in the direction of the local unit
   */
  protected static binSearchToTarget(element: number, table: UnitTable): number {
    return Unit.binSearch(element, table, Dir.ToOnset, Dir.ToUnit);
  }

  /**
   * A bin search in a table, in the direction of musical time
   */
  protected static binSearchToOnset(element: number, table: UnitTable): number {
    return Unit.binSearch(element, table, Dir.ToUnit, Dir.ToOnset);
  }

  /**
   * A binary search, returning the interpolated value
   * If a search value is out of the range, it will be clamped to the range
   * @param element the search value
   * @param table the table to search
   * @param from the index of the unit pair to use for the search
   * @param to the index of the unit pair to use for the return value
   * @param snap if the result should be interpolated or simply be the closest node
   * @returns A converted numerical value according to the table and direction
   */
  protected static binSearch(
    element: number,
    table: UnitTable,
    from: Dir,
    to: Dir,
    snap = false,
  ): number {
    const [prevIndex, nextIndex] = Unit.getBounds(element, table, from);

    if (!Number.isFinite(prevIndex)) return table[0][to];

    if (!Number.isFinite(nextIndex)) return table[table.length - 1][to];

    const prev = table[prevIndex];
    const next = table[nextIndex];

    const alpha = (element - prev[from]) / (next[from] - prev[from]);

    if (snap) return alpha <= 0.5 ? prev[to] : next[to];

    return prev[to] + (next[to] - prev[to]) * alpha;
  }

  /**
   * The binary search algorithm, returns an index range
   * @param element the search value
   * @param table the table to search
   * @param from the index of the unit pair to use for the search
   * @returns An ordered range, where one of the bounds may be infinite
   */
  protected static getBounds(element: number, table: UnitTable<number>, from: Dir):
  [number, number] {
    let first = 0;
    let last = table.length - 1;

    if (typeof element !== 'number') throw new Error('Searching with undefined value');

    if (element <= table[first][from]) {
      return [-Infinity, first];
    }
    if (element >= table[last][from]) {
      return [last, Infinity];
    }
    while (first < last - 1) {
      // Ratio optimization
      // Instead of looking for the middle like a binary search,
      // use the ratios of the nodes as a hint

      // Calculate where in the range we're looking
      const ratio = (table[first][from] - element) / (table[first][from] - table[last][from]);
      // Make sure that the ratio is between 0 and 1, and not NaN
      const clampedRatio = Math.min(1, Math.max(0, ratio || 0));
      // Calculate the index
      const index = Math.round(first + (last - first) * clampedRatio);
      // Clamp the index so that it cannot equal either end of the range
      const clampedIndex = Math.min(Math.max(index, first + 1), last - 1);
      // Get the row using the index
      const middle = table[clampedIndex];
      // Move the search range
      if (element > middle[from]) {
        first = clampedIndex;
      } else {
        last = clampedIndex;
      }
    }
    return [first, last];
  }

  getScaled(scale: number): Unit<Target> {
    return new Unit(this.tables, this.scale * scale);
  }

  /**
   * Create a new unit object that is faster and smaller
   * It however cannot be used for "snapping", as many nodes are removed
   * @returns a faster Unit<Target>
   */
  getOptimized(): Unit<Target> {
    return new Unit(this.tables.map(Unit.optimizeTable), this.scale);
  }

  /**
   * Check that a unit is an extension of a another unit (for waveforms)
   * @param track the possible parent unit
   * @returns true if this is a child of track
   */
  isChildOfUnit(parent: Unit<number>): boolean {
    return parent.tables.every((trackTable, index) => trackTable === this.tables[index]);
  }

  /**
   * Return a table with some rows removed
   * Rows are removed if they are very close to the interpolation of their neighbors
   * @param table
   * @returns
   */
  protected static optimizeTable(table: UnitTable): UnitTable {
    return table.filter((row, index, target) => {
      if (index === 0 || index === target.length - 1) return true;

      const prevRow = target[index - 1];
      const nextRow = target[index + 1];

      const interpolatedFrom = (prevRow[0] - row[0]) / (prevRow[0] - nextRow[0]);
      const interpolatedTo = prevRow[1] + (nextRow[1] - prevRow[1]) * interpolatedFrom;

      return Math.abs(interpolatedTo - row[1]) > 0.000001;
    });
  }
}
