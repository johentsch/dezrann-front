import type { AudioFileFormat } from '@/formats/AudioParser';
import AudioParser from '@/formats/AudioParser';
import type {
  AudioImageFileFormat, Image3DFileFormat, ImageDimensions, ImageFileFormat,
} from '@/formats/SourceParser';
import SourceParser from '@/formats/SourceParser';
import StaffParser from '@/formats/StaffParser';
import type {
  AudioImageSyncFormat, AudioSyncFormat, Image3DSyncFormat, ImageSyncFormat,
} from '@/formats/UnitParser';
import UnitParser from '@/formats/UnitParser';
import type {
  Audio, Pixel, Second, Source, Unit,
} from '@/data';
import { StaffType, SourceType } from '@/data';
import type { PieceFormat } from '@/formats/PieceParser';
import type { Image3DPageData } from '@/data/Source';
import { ClientResponse } from './ClientResponse';
import type { IClient } from './IClient';

async function loadImageDimensions(url: string): Promise<ImageDimensions> {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.onload = () => resolve({
      width: image.naturalWidth as Pixel,
      height: image.naturalHeight as Pixel,
    });
    image.onerror = reject;
    image.src = url;
  });
}

/**
 * Filter out an array of settles promises and log errors
 * @param result The output array of Promise.allSettled
 * @returns All successful results
 */
function filterSettled<T>(result: PromiseSettledResult<T>[]): T[] {
  return result.map((r) => {
    if (r.status === 'fulfilled') {
      return r.value;
    }
    // eslint-disable-next-line no-console
    console.error('Could not load source', r.reason);
    return null;
  }).filter((r) => !!r) as T[];
}

/**
 * Load an image source using info from the Piece object
 * @param this the dezrann client
 * @param format the format of the image info, from the SourceFormat
 * @param id the id of the piece
 * @returns a complete Source object
 */
async function loadImageSource(
  this: IClient,
  format: ImageFileFormat,
  id: string,
): Promise<Source> {
  const url = this.url(id, format.positions).data;

  const imageUrl = this.url(id, format.image).data;

  const syncFormat = (await this.get<ImageSyncFormat>(url)).data;

  const pixelUnit = UnitParser.createImageUnit(syncFormat);

  const dimensions = await loadImageDimensions(imageUrl);

  const staves = StaffParser.parseFormat(syncFormat.staffs, dimensions.height);

  return SourceParser
    .fromFormat(format, dimensions, pixelUnit, staves, imageUrl, undefined, [{
      height: dimensions.height,
      rows: [{
        staves,
        y: staves[0].top,
        height: staves[0].height,
        end: dimensions.width,
        start: 0 as Pixel,
      }],
    }]);
}

async function loadImage3DSource(
  this: IClient,
  format: Image3DFileFormat,
  id: string,
): Promise<Source> {
  const url = this.url(id, format.positions).data;
  const imageUrl = this.url(id, format.image).data;
  const syncFormat = (await this.get<Image3DSyncFormat>(url)).data;
  const pixel3DUnit = UnitParser.createImage3DUnit(syncFormat);
  let resourcesUrl = '';
  if (this.isDirectory) {
    resourcesUrl = this.url(`${id}/`).data;
  } else {
    resourcesUrl = `${this.analysesUrl}resources/${id}/sources/images/scan/`;
  }
  const dimensions = await loadImageDimensions(`${resourcesUrl}${syncFormat.pages[0].image}`);
  const staves = StaffParser.parseFormat([
    { top: 0, bottom: dimensions.height * (syncFormat.pages.length) },
  ], dimensions.height * (syncFormat.pages.length) as Pixel);

  const pages: Array<Image3DPageData> = [];
  syncFormat.pages.forEach((page, pageIndex) => {
    pages.push({
      image: `${resourcesUrl}${page.image}`,
      height: dimensions.height,
      rows: [],
    });
    page.rows.forEach((row) => {
      pages[pageIndex].rows.push({
        staves: [
          {
            top: row.y as Pixel,
            bottom: (row.y + row.height) as Pixel,
            height: row.height as Pixel,
            id: 0,
            type: StaffType.line,
            name: 'all',
          },
          {
            top: row.y as Pixel,
            bottom: (row.y + row.height / 3) as Pixel,
            height: row.height / 3 as Pixel,
            id: 7,
            type: StaffType.staff,
            name: 'default',
          },
          {
            top: (row.y + row.height / 3) as Pixel,
            bottom: (row.y + (row.height / 3) * 2) as Pixel,
            height: row.height / 3 as Pixel,
            id: 8,
            type: StaffType.staff,
            name: 'default',
          },
          {
            top: (row.y + (row.height / 3) * 2) as Pixel,
            bottom: (row.y + row.height) as Pixel,
            height: row.height / 3 as Pixel,
            id: 9,
            type: StaffType.staff,
            name: 'default',
          },
        ],
        y: row.y as Pixel,
        height: row.height as Pixel,
        end: row['end-x'] as Pixel,
        start: row['start-x'] as Pixel,
      });
    });
  });

  return SourceParser
    .fromFormat(format, dimensions, pixel3DUnit, staves, imageUrl, undefined, pages);
}

/**
 * Load an audio image source (spectrogram) using info from the Piece object
 * @param this the dezrann client
 * @param format the format of the audio image info, from the SourceFormat
 * @param secondUnit the onset-to-second unit of the parent track
 * @param id the id of the piece
 * @returns a complete Source object
 */
async function loadAudioImageSource(
  this: IClient,
  format: AudioImageFileFormat,
  track: Audio,
  id: string,
): Promise<Source> {
  const timeToPixelUrl = this.url(id, format.positions).data;

  const timeToPixel = (await this.get<AudioImageSyncFormat>(timeToPixelUrl)).data;

  const pixelUnit = UnitParser.createAudioImageUnit(track.secondUnit, timeToPixel);

  const imageUrl = this.url(id, format.image).data;

  const dimensions = await loadImageDimensions(imageUrl);

  const staves = StaffParser.parseFormat(timeToPixel.staffs, dimensions.height);

  return SourceParser.fromFormat(format, dimensions, pixelUnit, staves, imageUrl, track, [{
    height: dimensions.height,
    rows: [{
      staves,
      y: staves[0].top,
      height: staves[0].height,
      end: dimensions.width,
      start: 0 as Pixel,
    }],
  }]);
}

/**
 * Load an audio track using info from the piece object
 * @param this The dezrann Client
 * @param format the format of the audio info, from the SourceFormat
 * @param unit the onset-to-second unit of the track, pre-loaded by loadAudioSources
 * @param id of the piece
 * @returns
 */
function loadAudio(
  this: IClient,
  format: AudioFileFormat,
  secondUnit: Unit<Second>,
  id: string,
): Audio {
  let url: string | undefined;
  let ytId: string | undefined;

  // Using the object structure, load the audio id from the backend or youtube
  if ('yt-id' in format && format['yt-id']) {
    ytId = format['yt-id'];
  }
  if ('file' in format && format.file) {
    url = this.url(id, format.file).data;
  }

  if (!ytId && !url) {
    throw ClientResponse.fromParsingError(new Error('Invalid Audio Format: missing source'));
  }

  return AudioParser.fromFormat(secondUnit, url, ytId, format.info);
}

export interface AudioSources {
  track: Audio;
  images: Source[];
}
async function loadAudioSources(
  this: IClient,
  format: AudioFileFormat,
  id: string,
): Promise<AudioSources> {
  const audioSyncUrl = format['onset-date'];

  if (!audioSyncUrl) {
    throw ClientResponse.fromParsingError(new Error('Invalid Audio Format: no sync url'));
  }

  const onsetToTimeUrl = this.url(id, audioSyncUrl).data;

  const onsetToTime = (await this.get<AudioSyncFormat>(onsetToTimeUrl)).data;

  const secondUnit = UnitParser.createAudioUnit(onsetToTime);

  const track = loadAudio.bind(this)(format, secondUnit, id);

  const imagePromises = format
    ?.images
    ?.map((imageFormat) => {
      const newFormat = imageFormat;
      newFormat.license = format.license ?? '';
      return loadAudioImageSource.bind(this)(newFormat, track, id);
    })
    ?? [];

  const imagesSettled = await Promise.allSettled(imagePromises);

  const images = filterSettled(imagesSettled);

  return { images, track };
}

export interface PieceSources {
  sources: Source[];
  tracks: Audio[];
}

export async function loadPieceSources(
  this: IClient,
  format: PieceFormat,
  path: string,
): Promise<PieceSources> {
  // Get promises for all scores
  const scoreSourcePromises: Promise<Source>[] = format
    ?.sources
    ?.images
    ?.map((imageFormat) => {
      if (imageFormat.type === SourceType.Image3D) {
        return loadImage3DSource.bind(this)(imageFormat, path);
      }
      return loadImageSource.bind(this)(imageFormat, path);
    })
    ?? [];

  // Get promises for all audio files
  const audioSourcePromises: Promise<AudioSources>[] = format
    ?.sources
    ?.audios
    ?.map((audioFormat) => loadAudioSources.bind(this)(audioFormat, path))
    ?? [];

  // Await and filter results

  const scoresSourcesSettled = await Promise.allSettled(scoreSourcePromises);
  const scoreSources = filterSettled(scoresSourcesSettled);

  const audioSourceSettled = await Promise.allSettled(audioSourcePromises);
  const audioSources = filterSettled(audioSourceSettled);

  const tracks = audioSources.map((s) => s.track);

  const audioImageSources = audioSources.map((s) => s.images).flat();

  const sources = [
    ...scoreSources,
    ...audioImageSources,
  ];

  sources.forEach((s, index) => s.setId(index));

  return { sources, tracks };
}
