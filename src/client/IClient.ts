import type { HttpResponse, Response } from './ClientResponse';

export interface IClient {
  get<T>(url: string): Promise<HttpResponse<T>>;
  getFolder(url: string): Promise<Response<string[]>>;
  post<T>(url: string, data: unknown): Promise<HttpResponse<T>>;
  put<T>(url: string, data: unknown): Promise<HttpResponse<T>>

  tryParse<T>(fn: () => T): Response<T>;

  baseUrl: string;

  authUrl: string | null;

  analysesUrl: string | null;

  url(...components: string[]): Response<string>;
  newUrl(...components: string[]): Response<string>;

  changePwdUrl: string | null;

  isDirectory: boolean;
}
