import type { ChartConfiguration } from 'chart.js';
import { ChartJSNodeCanvas } from 'chartjs-node-canvas';

const createImage = async (data: Array<{ iteration: number, time: number }>): Promise<string> => {
  const configuration: ChartConfiguration = {
    type: 'line', // for line chart
    data: {
      labels: data.map((d) => d.iteration),
      datasets: [{
        label: 'Time [ms]',
        data: data.map((d) => d.time),
        fill: true,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.5,
      }],
    },
    options: {
      scales: {
        xAxis: {
          display: true,
          position: 'bottom',
          ticks: {
            maxRotation: 90,
          },
          title: {
            color: 'rgb(75, 192, 192)',
            display: true,
            text: 'Step',
          },
        },
        yAxis: {
          display: true,
          position: 'left',
          max: 1000,
          title: {
            color: 'rgb(75, 192, 192)',
            display: true,
            text: 'Time [ms]',
          },
        },
      },
    },
  };
  const canvasRenderService = new ChartJSNodeCanvas({ width: 400, height: 300, type: 'svg' });
  const buffer = canvasRenderService.renderToBufferSync(configuration, 'image/svg+xml');
  return buffer.toString();
};

export default createImage;
