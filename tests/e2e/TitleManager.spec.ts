import type { Locator, Page } from '@playwright/test';
import test, { expect } from '@playwright/test';

test.describe.skip('TitleManager.vue', () => {
  let previousArrow: Locator;
  let nextArrow: Locator;

  const expectTitlesToChange = async (
    page: Page,
    oldTitle1: string,
    oldTitle2: string,
    newTitle1: string,
    newTitle2: string,
    firstArrow: Locator,
    secondArrow: Locator,
  ) => {
    const pageUrl = page.url();
    if (!await firstArrow.getAttribute(':hover')) { return; }
    await firstArrow.hover();
    const oldTitleText1 = await page.locator(oldTitle1).textContent();
    const oldTitleText2 = await page.locator(oldTitle2).textContent();

    await firstArrow.click();
    await expect(page).toHaveURL(pageUrl);
    if (!await secondArrow.getAttribute(':hover')) {
      const newTitleText1 = await page.locator(newTitle1).textContent();
      expect(newTitleText1).toEqual(oldTitleText1);
      return;
    }
    await secondArrow.hover();
    const newTitleText1 = await page.locator(newTitle1).textContent();
    const newTitleText2 = await page.locator(newTitle2).textContent();

    expect(newTitleText1).toEqual(oldTitleText1);
    expect(newTitleText2).toEqual(oldTitleText2);
  };

  test.beforeEach(async ({ page }) => {
    await page.goto('/~/bach-fugues/bwv847');
    await (page.locator('span.v-btn__content div.nav-icon')).click();
    await page.waitForSelector('.change-piece');
    previousArrow = page.locator('.change-piece').first();
    nextArrow = page.locator('.change-piece').last();
  });

  test('Checks that all arrows are loaded', async ({ page }) => {
    const arrows = (await page.locator('.change-piece').all()).length;
    expect(arrows).toEqual(2);
  });

  test('Checks that each click on the previous arrow changes all titles and doesn\'t load another page', async ({ page }) => {
    await expectTitlesToChange(page, '#prev-title', '.real-title', '.real-title', '#next-title', previousArrow, nextArrow);
    await expectTitlesToChange(page, '#prev-title', '.real-title', '.real-title', '#next-title', previousArrow, nextArrow);
    await expectTitlesToChange(page, '#prev-title', '.real-title', '.real-title', '#next-title', previousArrow, nextArrow);
  });

  test('Checks that each click on the next arrow changes all titles and doesn\'t load another page', async ({ page }) => {
    await expectTitlesToChange(page, '#next-title', '.real-title', '.real-title', '#prev-title', nextArrow, previousArrow);
    await expectTitlesToChange(page, '#next-title', '.real-title', '.real-title', '#prev-title', nextArrow, previousArrow);
    await expectTitlesToChange(page, '#next-title', '.real-title', '.real-title', '#prev-title', nextArrow, previousArrow);
  });
});
