import test, { expect } from '@playwright/test';

test.describe('Labels', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/~~/bwv847');
    await page.waitForSelector('.label-rect');
  });

  test('Check page is loaded and labels are loaded', async ({ page }) => {
    const labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);
  });

  test('Create empty analysis', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    const labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(0);
  });

  test('Create a new label button', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    const newBtnSelector = '.creation-button.my-2.mx-2.v-btn.active.v-btn--active.v-btn--is-elevated.v-btn--has-bg.v-btn--rounded.theme--light.elevation-2.v-size--large.type-Pattern.tag-qwerty';

    expect(await page.locator(newBtnSelector).count()).toEqual(0);

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    expect(await page.locator(newBtnSelector).count()).toEqual(1);
  });

  test('Create a new label based on it', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    await page.keyboard.press('1');

    const labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(2);
  });

  test('Create a new label and place it on a wave', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    await page.locator('.v-icon.notranslate.mdi.mdi-playlist-music.theme--dark').first().click();
    await page.locator('.v-icon.notranslate.mdi.mdi-waveform.theme--light').first().click();

    await page.keyboard.press('1');

    const labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(2);
  });

  test('Move the label', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    await page.keyboard.press('1');

    const newLabel = await page.locator('.label-rect').first();
    const newLabelBB = await newLabel.boundingBox();

    expect(newLabelBB).toBeTruthy();

    await page.getByRole('tab', { name: 'Edit' }).click();

    expect(await page.getByLabel('Start').inputValue()).toBe('1');
    expect(await page.getByLabel('Duration').inputValue()).toBe('1');
    expect(await page.getByLabel('Offset').inputValue()).toBe('2');

    if (newLabelBB) {
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        (newLabelBB.x + newLabelBB.width / 2) + 253,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.up();

      expect(await page.getByLabel('Start').inputValue()).toBe('2');
      expect(await page.getByLabel('Duration').inputValue()).toBe('1');
      expect(await page.getByLabel('Offset').inputValue()).toBe('3');

      await page.mouse.move(
        (newLabelBB.x + newLabelBB.width / 2) + 253,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.up();

      expect(await page.getByLabel('Start').inputValue()).toBe('1');
      expect(await page.getByLabel('Duration').inputValue()).toBe('1');
      expect(await page.getByLabel('Offset').inputValue()).toBe('2');

      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        (newLabelBB.x + newLabelBB.width / 2) - newLabelBB.width / 2,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.up();

      expect(await page.getByLabel('Start').inputValue()).toBe('1');
      expect(await page.getByLabel('Duration').inputValue()).toBe('2/4');
      expect(await page.getByLabel('Offset').inputValue()).toBe('1+2/4');
    }
  });

  test('Resize the label', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    await page.keyboard.press('1');

    const newLabelLeft = await page.locator('.arrow.type-Pattern.tag-qwerty').first();
    const newLabelLeftBB = await newLabelLeft.boundingBox();

    const newLabelRight = await page.locator('.arrow.type-Pattern.tag-qwerty').last();
    const newLabelRightBB = await newLabelRight.boundingBox();

    expect(newLabelLeftBB).toBeTruthy();
    expect(newLabelRightBB).toBeTruthy();

    await page.getByRole('tab', { name: 'Edit' }).click();

    expect(await page.getByLabel('Start').inputValue()).toBe('1');
    expect(await page.getByLabel('Duration').inputValue()).toBe('1');
    expect(await page.getByLabel('Offset').inputValue()).toBe('2');

    if (newLabelLeftBB && newLabelRightBB) {
      await page.mouse.move(
        newLabelRightBB.x + newLabelRightBB.width / 2,
        newLabelRightBB.y + newLabelRightBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        (newLabelRightBB.x + newLabelRightBB.width / 2) - 132,
        newLabelRightBB.y + newLabelRightBB.height / 2,
      );
      await page.mouse.up();

      expect(await page.getByLabel('Start').inputValue()).toBe('1');
      expect(await page.getByLabel('Duration').inputValue()).toBe('2/4');
      expect(await page.getByLabel('Offset').inputValue()).toBe('1+2/4');

      await page.mouse.move(
        newLabelLeftBB.x + newLabelLeftBB.width / 2,
        newLabelLeftBB.y + newLabelLeftBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        (newLabelLeftBB.x + newLabelLeftBB.width / 2) + 93,
        newLabelLeftBB.y + newLabelLeftBB.height / 2,
      );
      await page.mouse.up();

      expect(await page.getByLabel('Start').inputValue()).toBe('1+3/8');
      expect(await page.getByLabel('Duration').inputValue()).toBe('1/8');
      expect(await page.getByLabel('Offset').inputValue()).toBe('1+2/4');

      await page.mouse.move(
        (newLabelLeftBB.x + newLabelLeftBB.width / 2) + 93,
        newLabelLeftBB.y + newLabelLeftBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        (newLabelLeftBB.x + newLabelLeftBB.width / 2) + 250,
        newLabelLeftBB.y + newLabelLeftBB.height / 2,
      );
      await page.mouse.up();
    }

    expect(await page.getByLabel('Start').inputValue()).toBe('1+2/4');
    expect(await page.getByLabel('Duration').inputValue()).toBe('2/4');
    expect(await page.getByLabel('Offset').inputValue()).toBe('2');
  });

  test('Change Staff', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    await page.keyboard.press('1');

    const newLabel = await page.locator('.label-rect').first();
    const newLabelBB = await newLabel.boundingBox();

    expect(newLabelBB).toBeTruthy();

    const staves = await page.locator('.staff');
    expect(await staves.count()).toBe(20);

    expect(parseInt(await newLabel.getAttribute('y') || '', 10))
      .toEqual(parseInt(await staves.nth(7).getAttribute('y') || '', 10) - 1);

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-chevron-double-down.theme--light').click();

    if (newLabelBB) {
      // Staff 2
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        newLabelBB.y + newLabelBB.height / 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + 108,
      );
      await page.mouse.up();
      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(8).getAttribute('y') || '', 10) - 1);

      // Staff 3
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + 108,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + 108 * 2,
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(9).getAttribute('y') || '', 10) - 1);

      // Staff Top.3
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + 108 * 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16),
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(3).getAttribute('y') || '', 10) - 1);

      // Staff Top.2
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16),
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16) - 32,
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(2).getAttribute('y') || '', 10) - 1);

      // Staff Top.1
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16) - 32,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16) - (32 * 2),
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(1).getAttribute('y') || '', 10) - 1);

      // Staff Bottom.1
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16) - (32 * 2),
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + (108 * 2) + (newLabelBB.height / 2 + 16),
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(4).getAttribute('y') || '', 10) - 1);

      // Staff Bottom.2
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + (108 * 2) + (newLabelBB.height / 2 + 16),
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + (108 * 2) + (newLabelBB.height / 2 + 16) + 32,
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(5).getAttribute('y') || '', 10) - 1);

      // Staff Bottom.3
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + (108 * 2) + (newLabelBB.height / 2 + 16) + 32,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + (108 * 2) + (newLabelBB.height / 2 + 16) + 32 * 2,
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(6).getAttribute('y') || '', 10) - 1);

      // Bottom.3 -> Top.1
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) + (108 * 2) + (newLabelBB.height / 2 + 16) + 32 * 2,
      );
      await page.mouse.down();
      await page.mouse.move(
        newLabelBB.x + newLabelBB.width / 2,
        (newLabelBB.y + newLabelBB.height / 2) - (newLabelBB.height / 2 + 16) - (32 * 2),
      );
      await page.mouse.up();

      expect(parseInt(await newLabel.getAttribute('y') || '', 10))
        .toEqual(parseInt(await staves.nth(1).getAttribute('y') || '', 10) - 1);
    }
    expect(await page.locator('.label-rect').count()).toEqual(2);
  });

  test('Delete the label', async ({ page }) => {
    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-plus.theme--light').click();

    const addSelector = '.fab.v-btn--round.v-btn.v-btn--bottom.v-btn--is-elevated.v-btn--has-bg.v-btn--right.theme--light.elevation-2.v-size--small.secondary';
    await page.waitForSelector(addSelector);

    await page.locator(addSelector).click();

    await page.keyboard.insertText('qwerty');
    await page.keyboard.press('Enter');

    await page.keyboard.press('1');
    expect(await page.locator('.label-rect').count()).toEqual(2);

    await page.keyboard.press('Delete');
    expect(await page.locator('.label-rect').count()).toEqual(0);
  });

  test('Create a vertical label', async ({ page }) => {
    const label = await page.locator('.label-rect').first();
    const labelBB = await label.boundingBox();
    expect(labelBB).toBeTruthy();

    const addIconBtn = await page.locator('.v-icon.notranslate.mdi.mdi-plus').first();
    await addIconBtn.click();

    await page.waitForTimeout(1000);

    if (labelBB) {
      await page.mouse.move(
        labelBB.x + labelBB.width,
        (labelBB.y + labelBB.height / 2) - 108,
      );
      await page.mouse.down();
      await page.mouse.move(
        labelBB.x + labelBB.width,
        (labelBB.y + labelBB.height / 2) + 108,
        { steps: 4 },
      );
      await page.mouse.up();
    }

    const newLabel = await page.locator('.label-rect').first();
    expect(await newLabel.getAttribute('y')).toEqual('1');
    expect(await newLabel.getAttribute('height')).toEqual('497');
  });
});
