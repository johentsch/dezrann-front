/* eslint-disable @typescript-eslint/no-var-requires */
import type { Duration, Onset } from '@/data';
import { Signature } from '@/data';
import type { TMeasureMap } from '@/data/MeasureMap';

const K283_1 = require('./measure-maps/K283-1.mm.json');
const K284_3 = require('./measure-maps/K284-3.mm.json');
const K331_1 = require('./measure-maps/K331-1.mm.json');

/**
 * Describes the behavior of the time signature conversion object
 *
 * Previous version: https://gitlab.com/algomus.fr/dezrann/-/blob/d5e4a6058bc88440256b513d4bfe9d32fdbc43a6/code/essais/sc-client/test/test.js
 *
 * Issue: https://gitlab.com/algomus.fr/dezrann/-/issues/342
 */
describe('Signature', () => {
  const signature2b2 = new Signature({ numerator: 2, denominator: 2 });

  const signature4 = new Signature({ numerator: 4, denominator: 4 });
  const signature3 = new Signature({ numerator: 3, denominator: 4 });

  const signature3b8 = new Signature({ numerator: 3, denominator: 8 });
  const signature12b8 = new Signature({ numerator: 12, denominator: 8 });

  const signature4a2 = new Signature({ numerator: 4, denominator: 4, upbeat: 2 });
  const signature2a2 = new Signature({ numerator: 2, denominator: 2, upbeat: 2 });

  it('beatIncrement()', () => {
    expect(signature2b2.beatIncrement).toEqual(2);
    expect(signature4.beatIncrement).toEqual(1);
    expect(signature3.beatIncrement).toEqual(1);
    expect(signature3b8.beatIncrement).toEqual(1.5);
  });

  it('printDuration()', () => {
    expect(signature2b2.printDuration(1.5 as Duration)).toEqual('3/8');
    expect(signature2b2.printDuration(56.0 as Duration)).toEqual('14');

    expect(signature4.printDuration(1.5 as Duration)).toEqual('3/8');
    expect(signature4.printDuration(56.0 as Duration)).toEqual('14');
    expect(signature4.printDuration(57.0 as Duration)).toEqual('14+1/4');
    expect(signature4.printDuration(58.667 as Duration)).toEqual('14+8/12');
    expect(signature4.printDuration(56.492 as Duration)).toEqual('14.123');
    expect(signature4.printDuration(3.75 as Duration)).toEqual('1-1/16');
    // 1/16 is always a sixteenth, even in 3/4
    expect(signature3.printDuration(2.75 as Duration)).toEqual('1-1/16');
    expect(signature3.printDuration(44.0 as Duration)).toEqual('14+2/4');

    expect(signature3b8.printDuration(1.5 as Duration)).toEqual('1');
    expect(signature12b8.printDuration(3 as Duration)).toEqual('6/8');
    expect(signature12b8.printDuration(10.5 as Duration)).toEqual('2-3/8');
    expect(signature12b8.printDuration(11.5 as Duration)).toEqual('2-1/8');
  });

  it('parseDuration()', () => {
    expect(signature4.parseDuration('14+1/4')).toEqual(57.0);
    expect(signature4.parseDuration('14-1/4')).toEqual(55.0);
    expect(signature4.parseDuration('14')).toEqual(56.0);
    expect(signature4.parseDuration('14.4')).toEqual(57.6);
    expect(signature3.parseDuration('14+1/4')).toEqual(43.0);
    expect(signature4.parseDuration('16/16')).toEqual(4.0);
  });

  it('printOnset()', () => {
    expect(signature4.printOnset(57.0 as Onset)).toEqual('15+1/4');
  });

  it('parseOnset()', () => {
    expect(signature4.parseOnset('15+1/4')).toEqual(57.0);
  });

  it('printOnset() upbeat = 2', () => {
    expect(signature4a2.printOnset(0.0 as Onset)).toEqual('1-2/4');
    expect(signature4a2.printOnset(57.0 as Onset)).toEqual('15-1/4');
    expect(signature2a2.printOnset(6 as Onset)).toEqual('2');
  });

  it('parseOnset() upbeat = 2', () => {
    expect(signature4a2.parseOnset('15-1/4')).toEqual(57.0);
  });
});

describe('Signature, with map', () => {
  const VivaldiSummerIMap: TMeasureMap = [
    {
      count: 1,
      qstamp: 0,
      number: 1,
      time_signature: '3/8',
      nominal_length: 1.5,
      actual_length: 1.5,
      start_repeat: false,
      end_repeat: false,
      next: [2],
    },
    {
      count: 32,
      qstamp: 46.5,
      number: 32,
      time_signature: '4/4',
      nominal_length: 4,
      actual_length: 4,
      start_repeat: false,
      end_repeat: false,
      next: [33],
    },
    {
      count: 53,
      qstamp: 130.5,
      number: 53,
      time_signature: '3/8',
      nominal_length: 1.5,
      actual_length: 1.5,
      start_repeat: false,
      end_repeat: false,
      next: [],
    },
    // { onset: 0, 'time-signature': '3/8' },
    // { onset: 46.5, 'bar-number': 32, 'time-signature': '4/4' },
    // { onset: 130.5, 'time-signature': '3/8' },
  ];
  const map1 = new Signature(VivaldiSummerIMap);

  it('printOnset()', () => {
    // expect(map1.printOnset(0.0 as Onset)).toEqual('1');

    expect(map1.printOnset(44.5 as Onset)).toEqual('30+2/8');
    expect(map1.printOnset(45.0 as Onset)).toEqual('31');
    expect(map1.printOnset(50.0 as Onset)).toEqual('33-1/8');

    expect(map1.printOnset(130.0 as Onset)).toEqual('53-1/8');
    expect(map1.printOnset(130.5 as Onset)).toEqual('53');
    expect(map1.printOnset(132.0 as Onset)).toEqual('54');
  });

  it('printDuration()', () => {
    expect(map1.printDuration(1.5 as Duration)).toEqual('1');
  });

  it('printOnset() 2', () => {
    // const MapOverrides: MeasureMap = {
    //   meter: [
    //     { onset: 0, 'time-signature': '3/8' },
    //     // Bar number overriden to 40
    //     { onset: 20, 'bar-number': 40, 'time-signature': '4/4' },
    //     // Bar 40+10=50, with a .5 upbeat, thus strong beat of 50 is at onset 60.5
    //     { onset: 60, 'time-signature': '3/8', 'time-signature-upbeat': 0.5 },
    //   ],
    // };

    // const map2 = new Signature(MapOverrides);

    // it('printOnset()', () => {
    //   expect(map2.printOnset(0.0 as Onset)).toEqual('1');

    //   expect(map2.printOnset(19.0 as Onset)).toEqual('7+1/8');
    //   expect(map2.printOnset(19.5 as Onset)).toEqual('40-1/8');
    //   expect(map2.printOnset(20.0 as Onset)).toEqual('40');
    //   expect(map2.printOnset(25.0 as Onset)).toEqual('41+1/4');

    //   expect(map2.printOnset(59.5 as Onset)).toEqual('50-2/8'); // Or 49+7/8 ?
    //   expect(map2.printOnset(60.0 as Onset)).toEqual('50-1/8');
    //   expect(map2.printOnset(60.5 as Onset)).toEqual('50');
    //   expect(map2.printOnset(62.0 as Onset)).toEqual('51');
    // });

    const testMap: TMeasureMap = [
      {
        actual_length: 4,
        count: 0,
        end_repeat: false,
        next: [
          2,
        ],
        nominal_length: 4,
        number: 1,
        qstamp: 0,
        start_repeat: false,
        time_signature: '4/4',
      },
      {
        actual_length: 4,
        count: 33,
        end_repeat: false,
        next: [
          35,
        ],
        nominal_length: 4,
        number: 34,
        qstamp: 132,
        start_repeat: false,
        time_signature: '4/4',
      },
      {
        actual_length: 2,
        count: 34,
        end_repeat: false,
        next: [
          36,
        ],
        nominal_length: 2,
        number: 35,
        qstamp: 136,
        start_repeat: false,
        time_signature: '2/4',
      },
      {
        actual_length: 4,
        count: 35,
        end_repeat: false,
        next: [
          37,
        ],
        nominal_length: 4,
        number: 36,
        qstamp: 138,
        start_repeat: false,
        time_signature: '4/4',
      },
      {
        actual_length: 4,
        count: 53,
        end_repeat: false,
        next: [],
        nominal_length: 4,
        number: 54,
        qstamp: 210,
        start_repeat: false,
        time_signature: '4/4',
      },
    ];

    const map3 = new Signature(testMap);

    expect(map3.printOnset(132 as Onset)).toEqual('34');
    expect(map3.printOnset(136 as Onset)).toEqual('35');
    expect(map3.printOnset(138 as Onset)).toEqual('36');

    // ### K283-1
    // 0   1-1/4
    // 13  5
    // 358 120

    // ### K331-1
    // 6    3
    // 294.5  98+1/8     # en fait 98b+1/8
    // 330   109
    // 338   111

    // ### K284-3
    // 0    1-2/4
    // 2    1
    // 880  220+2/4
    // 883  221+1/4
    // 884  222-1/4   # (ou 221b)
    // 894  225
    // 999  260
    const k283Map = new Signature(K283_1);
    expect(k283Map.printOnset(0 as Onset)).toEqual('1-1/4');
    expect(k283Map.printOnset(13 as Onset)).toEqual('5');
    expect(k283Map.printOnset(358 as Onset)).toEqual('120');

    const k331Map = new Signature(K331_1);
    expect(k331Map.printOnset(6 as Onset)).toEqual('3');
    expect(k331Map.printOnset(294.5 as Onset)).toEqual('98b+1/8');
    expect(k331Map.printOnset(330 as Onset)).toEqual('109');
    expect(k331Map.printOnset(338 as Onset)).toEqual('111');

    const k284Map = new Signature(K284_3);
    expect(k284Map.printOnset(0 as Onset)).toEqual('1-2/4');
    expect(k284Map.printOnset(2 as Onset)).toEqual('1');
    expect(k284Map.printOnset(880 as Onset)).toEqual('220+2/4');
    expect(k284Map.printOnset(883 as Onset)).toEqual('221a+1/4');
    expect(k284Map.printOnset(884 as Onset)).toEqual('221b');
    expect(k284Map.printOnset(894 as Onset)).toEqual('225');
    expect(k284Map.printOnset(999 as Onset)).toEqual('260');
  });
});
