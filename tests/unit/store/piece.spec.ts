/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable global-require */
import { Piece } from '@/data';
import type { PieceFormat } from '@/formats/PieceParser';
import PieceParser from '@/formats/PieceParser';
import Vue from 'vue';

import type { LocaleMessages } from 'vue-i18n';
import VueI18n from 'vue-i18n';

describe('Piece', () => {
  it('compareOpus()', () => {
    const pieces: Array<Partial<Piece>> = [
      { title: 't1', opus: { order: '3' } },
      { title: 't2', opus: { order: '3' } },
      { title: 't', opus: { opus: '14', 'movement:num': '1' } },
      { title: 't', opus: { opus: '14', 'movement:num': '2' } },
      { title: 't', opus: { opus: 'op67' } },
      { title: 't', opus: { opus: 'op68', 'movement:num': '7' } },
      { title: 't', opus: { opus: 'op68', 'movement:num': '42' } },
      { title: 't', opus: { opus: 'op68' } },
      { title: 't', opus: { contributors: { artist: 'John' } } },
    ];

    for (let i = 0; i < pieces.length; i += 1) {
      for (let j = 0; j < pieces.length; j += 1) {
        if (i === j) {
          expect(Piece.compareOpus(pieces[i], pieces[j])).toStrictEqual(0);
        } else if (i < j) {
          expect(Piece.compareOpus(pieces[i], pieces[j])).toStrictEqual(-1);
        } else {
          expect(Piece.compareOpus(pieces[i], pieces[j])).toStrictEqual(1);
        }
      }
    }
  });

  it('getKey()', () => {
    const pieceFormat: PieceFormat = {
      id: '',
      title: '',
      opus: { key: 'C # major' },
      sources: {},
    };
    const frMusicLocaleMessages = require('../../../src/locales/fr/music.json');
    const slMusicLocaleMessages = require('../../../src/locales/sl/music.json');
    const messages: LocaleMessages = {
      'fr-FR': frMusicLocaleMessages,
      'sl-SL': slMusicLocaleMessages,
    };

    Vue.use(VueI18n);

    const i18n = new VueI18n({
      locale: process.env.VUE_APP_I18N_LOCALE || 'en',
      fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
      messages,
    });

    let piece: Piece = PieceParser.fromFormat('', true, pieceFormat);
    i18n.locale = 'fr-FR';
    expect(piece.getKey(i18n)).toBe('do♯ majeur');
    i18n.locale = 'sl-SL';
    expect(piece.getKey(i18n)).toBe('C♯ dur');

    pieceFormat.opus.key = 'B♭ major';
    piece = PieceParser.fromFormat('', true, pieceFormat);
    i18n.locale = 'fr-FR';
    expect(piece.getKey(i18n)).toBe('si♭ majeur');
    i18n.locale = 'sl-SL';
    expect(piece.getKey(i18n)).toBe('B dur');

    pieceFormat.opus.key = 'Bb minor';
    piece = PieceParser.fromFormat('', true, pieceFormat);
    i18n.locale = 'fr-FR';
    expect(piece.getKey(i18n)).toBe('si♭ mineur');
    i18n.locale = 'sl-SL';
    expect(piece.getKey(i18n)).toBe('B mol');

    pieceFormat.opus.key = 'B major';
    piece = PieceParser.fromFormat('', true, pieceFormat);
    i18n.locale = 'fr-FR';
    expect(piece.getKey(i18n)).toBe('si majeur');
    i18n.locale = 'sl-SL';
    expect(piece.getKey(i18n)).toBe('H dur');
  });
});
