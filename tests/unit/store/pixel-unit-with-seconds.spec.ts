/* eslint-disable @typescript-eslint/no-var-requires */
import type { AudioImageSyncFormat, AudioSyncFormat } from '@/formats/UnitParser';
import UnitFormat from '@/formats/UnitParser';
import type { Onset, Pixel, Second } from '@/data';

describe('Unit', () => {
  /**
   * Here are described the behavior of the onset-pixel conversion object
   * Previous version: https://gitlab.com/algomus.fr/dezrann/-/blob/d5e4a6058bc88440256b513d4bfe9d32fdbc43a6/code/essais/sc-client/test/test.js
   * Issue: https://gitlab.com/algomus.fr/dezrann/-/issues/342
   */

  // A fake onset - second conversion format
  const audioSyncFile: AudioSyncFormat = [
    { onset: 0.0, date: 0.0 },
    { onset: 1.0, date: 0.1 },
    { onset: 2.0, date: 0.2 },
    { onset: 3.0, date: 0.3 },
    { onset: 4.0, date: 0.4 },
    { onset: 5.0, date: 0.5 },
    { onset: 6.0, date: 0.6 },
    { onset: 7.0, date: 0.7 },
    { onset: 8.0, date: 0.8 },
    { onset: 9.0, date: 0.9 },
  ];
  // A fake second-pixel conversion format
  const dateToX = [
    { date: 0.1, x: 10 },
    { date: 0.2, x: 20 },
    { date: 0.3, x: 30 },
    { date: 0.4, x: 40 },
    { date: 0.5, x: 50 },
    { date: 0.6, x: 60 },
    { date: 0.7, x: 70 },
    { date: 0.8, x: 80 },
    { date: 0.9, x: 90 },
  ];

  const audioImageSyncFile: AudioImageSyncFormat = {
    'date-x': dateToX,
    staffs: [],
  };

  const secondUnit = UnitFormat.createAudioUnit(audioSyncFile);
  const pixelUnit = UnitFormat.createAudioImageUnit(secondUnit, audioImageSyncFile);

  describe('Partial apply combine to full apply', () => {
    it('should work toward onset', () => {
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(10 as Pixel))).toEqual(1);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(20 as Pixel))).toEqual(2);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(30 as Pixel))).toEqual(3);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(40 as Pixel))).toEqual(4);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(50 as Pixel))).toEqual(5);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(60 as Pixel))).toEqual(6);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(70 as Pixel))).toEqual(7);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(80 as Pixel))).toEqual(8);
      expect(pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(90 as Pixel))).toEqual(9);
    });
    it('should work towards pixel', () => {
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(1 as Onset))).toEqual(10);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(2 as Onset))).toEqual(20);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(3 as Onset))).toEqual(30);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(4 as Onset))).toEqual(40);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(5 as Onset))).toEqual(50);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(6 as Onset))).toEqual(60);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(7 as Onset))).toEqual(70);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(8 as Onset))).toEqual(80);
      expect(pixelUnit.fromSecondToPixel(pixelUnit.fromOnsetToSecond(9 as Onset))).toEqual(90);
    });
    it('should work second toward onset', () => {
      expect(pixelUnit.fromSecondToOnset(0.1 as Second)).toEqual(1);
      expect(pixelUnit.fromSecondToOnset(0.2 as Second)).toEqual(2);
      expect(pixelUnit.fromSecondToOnset(0.3 as Second)).toEqual(3);
      expect(pixelUnit.fromSecondToOnset(0.4 as Second)).toEqual(4);
      expect(pixelUnit.fromSecondToOnset(0.5 as Second)).toEqual(5);
      expect(pixelUnit.fromSecondToOnset(0.6 as Second)).toEqual(6);
      expect(pixelUnit.fromSecondToOnset(0.7 as Second)).toEqual(7);
      expect(pixelUnit.fromSecondToOnset(0.8 as Second)).toEqual(8);
      expect(pixelUnit.fromSecondToOnset(0.9 as Second)).toEqual(9);
    });
    it('should work onset toward second', () => {
      expect(pixelUnit.fromOnsetToSecond(1 as Onset)).toEqual(0.1);
      expect(pixelUnit.fromOnsetToSecond(2 as Onset)).toEqual(0.2);
      expect(pixelUnit.fromOnsetToSecond(3 as Onset)).toEqual(0.3);
      expect(pixelUnit.fromOnsetToSecond(4 as Onset)).toEqual(0.4);
      expect(pixelUnit.fromOnsetToSecond(5 as Onset)).toEqual(0.5);
      expect(pixelUnit.fromOnsetToSecond(6 as Onset)).toEqual(0.6);
      expect(pixelUnit.fromOnsetToSecond(7 as Onset)).toEqual(0.7);
      expect(pixelUnit.fromOnsetToSecond(8 as Onset)).toEqual(0.8);
      expect(pixelUnit.fromOnsetToSecond(9 as Onset)).toEqual(0.9);
    });
    it('should work second toward pixel', () => {
      expect(pixelUnit.fromSecondToPixel(0.1 as Second)).toEqual(10);
      expect(pixelUnit.fromSecondToPixel(0.2 as Second)).toEqual(20);
      expect(pixelUnit.fromSecondToPixel(0.3 as Second)).toEqual(30);
      expect(pixelUnit.fromSecondToPixel(0.4 as Second)).toEqual(40);
      expect(pixelUnit.fromSecondToPixel(0.5 as Second)).toEqual(50);
      expect(pixelUnit.fromSecondToPixel(0.6 as Second)).toEqual(60);
      expect(pixelUnit.fromSecondToPixel(0.7 as Second)).toEqual(70);
      expect(pixelUnit.fromSecondToPixel(0.8 as Second)).toEqual(80);
      expect(pixelUnit.fromSecondToPixel(0.9 as Second)).toEqual(90);
    });
    it('should work pixel toward second', () => {
      expect(pixelUnit.fromPixelToSecond(10 as Pixel)).toEqual(0.1);
      expect(pixelUnit.fromPixelToSecond(20 as Pixel)).toEqual(0.2);
      expect(pixelUnit.fromPixelToSecond(30 as Pixel)).toEqual(0.3);
      expect(pixelUnit.fromPixelToSecond(40 as Pixel)).toEqual(0.4);
      expect(pixelUnit.fromPixelToSecond(50 as Pixel)).toEqual(0.5);
      expect(pixelUnit.fromPixelToSecond(60 as Pixel)).toEqual(0.6);
      expect(pixelUnit.fromPixelToSecond(70 as Pixel)).toEqual(0.7);
      expect(pixelUnit.fromPixelToSecond(80 as Pixel)).toEqual(0.8);
      expect(pixelUnit.fromPixelToSecond(90 as Pixel)).toEqual(0.9);
    });
    it('should work same as Unit<second> second toward onset', () => {
      expect(pixelUnit.fromSecondToOnset(-10 as Second)).toEqual(secondUnit.toOnset(-10 as Second));
      expect(pixelUnit.fromSecondToOnset(0 as Second)).toEqual(secondUnit.toOnset(0 as Second));
      expect(pixelUnit.fromSecondToOnset(0.1 as Second)).toEqual(secondUnit.toOnset(0.1 as Second));
      expect(pixelUnit.fromSecondToOnset(0.2 as Second)).toEqual(secondUnit.toOnset(0.2 as Second));
      expect(pixelUnit.fromSecondToOnset(0.3 as Second)).toEqual(secondUnit.toOnset(0.3 as Second));
      expect(pixelUnit.fromSecondToOnset(0.4 as Second)).toEqual(secondUnit.toOnset(0.4 as Second));
      expect(pixelUnit.fromSecondToOnset(0.5 as Second)).toEqual(secondUnit.toOnset(0.5 as Second));
      expect(pixelUnit.fromSecondToOnset(0.6 as Second)).toEqual(secondUnit.toOnset(0.6 as Second));
      expect(pixelUnit.fromSecondToOnset(0.7 as Second)).toEqual(secondUnit.toOnset(0.7 as Second));
      expect(pixelUnit.fromSecondToOnset(0.8 as Second)).toEqual(secondUnit.toOnset(0.8 as Second));
      expect(pixelUnit.fromSecondToOnset(0.9 as Second)).toEqual(secondUnit.toOnset(0.9 as Second));
      expect(pixelUnit.fromSecondToOnset(100 as Second)).toEqual(secondUnit.toOnset(100 as Second));
    });
    it('should work same as Unit<second> onset toward second', () => {
      expect(pixelUnit.fromOnsetToSecond(-10 as Onset)).toEqual(secondUnit.fromOnset(-10 as Onset));
      expect(pixelUnit.fromOnsetToSecond(0 as Onset)).toEqual(secondUnit.fromOnset(0 as Onset));
      expect(pixelUnit.fromOnsetToSecond(1 as Onset)).toEqual(secondUnit.fromOnset(1 as Onset));
      expect(pixelUnit.fromOnsetToSecond(2 as Onset)).toEqual(secondUnit.fromOnset(2 as Onset));
      expect(pixelUnit.fromOnsetToSecond(3 as Onset)).toEqual(secondUnit.fromOnset(3 as Onset));
      expect(pixelUnit.fromOnsetToSecond(4 as Onset)).toEqual(secondUnit.fromOnset(4 as Onset));
      expect(pixelUnit.fromOnsetToSecond(5 as Onset)).toEqual(secondUnit.fromOnset(5 as Onset));
      expect(pixelUnit.fromOnsetToSecond(6 as Onset)).toEqual(secondUnit.fromOnset(6 as Onset));
      expect(pixelUnit.fromOnsetToSecond(7 as Onset)).toEqual(secondUnit.fromOnset(7 as Onset));
      expect(pixelUnit.fromOnsetToSecond(8 as Onset)).toEqual(secondUnit.fromOnset(8 as Onset));
      expect(pixelUnit.fromOnsetToSecond(9 as Onset)).toEqual(secondUnit.fromOnset(9 as Onset));
      expect(pixelUnit.fromOnsetToSecond(100 as Onset)).toEqual(secondUnit.fromOnset(100 as Onset));
    });
  });
});
