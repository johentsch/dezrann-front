/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from 'vue';
import Vuetify from 'vuetify';

import { shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import TypeField from '@/components/toolbars/editing/editor-fields/TypeField.vue';
import TagField from '@/components/toolbars/editing/editor-fields/TagField.vue';
import CommentField from '@/components/toolbars/editing/editor-fields/CommentField.vue';
import OnsetField from '@/components/toolbars/editing/editor-fields/OnsetField.vue';
import OffsetField from '@/components/toolbars/editing/editor-fields/OffsetField.vue';
import DurationField from '@/components/toolbars/editing/editor-fields/DurationField.vue';

import type { Label, Duration, Onset } from '@/data';
import { Signature, defaultTypes } from '@/data';

import { mockActions } from '../behaviors/utils';

// TODO use createLocalVue
Vue.use(Vuetify);
Vue.use(VueI18n);

describe('LabelField.vue', () => {
  it('check that initializing fields does not produce label updates', async () => {
    const actions = mockActions();

    const label: Label = {
      comment: 'test',
      duration: 0 as Duration,
      durationType: 'ioi',
      id: 0,
      layers: [],
      onset: 0 as Onset,
      staffId: 0,
      tag: 'test',
      wasStaffLoaded: true,
      type: 'test',
    };

    const signature = new Signature();
    const types = [...defaultTypes.getTypeNames()];

    const i18n = new VueI18n({ locale: 'en-US', missing: () => '' });

    const options = {
      vuetify: new Vuetify(),
      i18n,
      propsData: {
        label, actions, types, signature,
      },
    };

    // Mount all fields
    const type = shallowMount(TypeField, options);
    const tag = shallowMount(TagField, options);
    const comment = shallowMount(CommentField, options);
    const on = shallowMount(OnsetField, options);
    const off = shallowMount(OffsetField, options);
    const dur = shallowMount(DurationField, options);

    // Wait for all updates
    await Vue.nextTick();

    // Check update was not called
    expect(actions.updateLabel).toBeCalledTimes(0);

    // Update the fields
    label.onset = 1 as Onset;
    label.duration = 12 as Duration;

    // Wait for all updates
    await Vue.nextTick();

    // Check that the fields have all updated
    expect((on.vm as any).field).toContain(signature.printOnset(label.onset));
    expect((dur.vm as any).field).toContain(signature.printDuration(label.duration));
    expect((off.vm as any).field).toContain(
      signature.printOnset((label.onset + label.duration) as Onset),
    );

    // Check update was not called even after field update
    expect(actions.updateLabel).toBeCalledTimes(0);
  });
});
