import { Signature, FilterGroup } from '@/data';
import LabelParser from '@/formats/LabelParser';
import type { LabelFormat } from '@/formats/analysis-formats';

describe('LabelParser.ts', () => {
  const example1 = {
    type: 'Pattern', tag: '', start: '4', end: 11,
  };
  const example2 = {
    type: 'Pattern', tag: '', start: 'm2', end: 'm4-1/4',
  };
  const example3 = {
    type: 'Pattern', tag: '', start: '4', ioi: '7',
  };
  const example4 = {
    type: 'Pattern', tag: '', start: 4, duration: 7,
  };
  const example5 = {
    type: 'Pattern', tag: '', start: 'm2', ioi: '1m+3/4',
  };
  const noFilter = new FilterGroup([]);

  const labelTest = (
    example: LabelFormat,
    durationType = 'ioi',
    signature = new Signature(),
    duration = 7,
    onset = 4,
  ) => {
    expect(LabelParser.fromFormat(example, noFilter, signature)).toEqual({
      comment: '', duration, durationType, layers: ['no-layer'], onset, staffId: 5, tag: '', type: 'Pattern', wasStaffLoaded: false,
    });
  };
  it('parsing a label : start (string without "m") and end (number)', () => labelTest(
    example1,
    'duration',
  ));
  it('parsing a label : start (string with "m") and end (string with "m")', () => labelTest(
    example2,
    'duration',
  ));
  it('parsing a label : start (string with "m") and end (string with "m"); signature 3/4', () => labelTest(
    example2,
    'duration',
    new Signature({ numerator: 3, denominator: 4 }),
    5,
    3,
  ));
  it('parsing a label : start (string without "m") and ioi (string without "m")', () => labelTest(
    example3,
  ));
  it('parsing a label : start (number) and duration (number)', () => labelTest(
    example4,
    'duration',
  ));
  it('parsing a label : start (string with "m") and ioi (string with "m")', () => labelTest(
    example5,
  ));
});
