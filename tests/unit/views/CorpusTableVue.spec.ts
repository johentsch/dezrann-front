import { mount, shallowMount } from '@vue/test-utils';
import CorpusTableVue from '../../../src/views/CorpusTableVue.vue';

jest.mock('@/components/utils/nav-tools');
describe('CorpusTableVue.vue', () => {
  it('should mount', () => {
    const wrapper = shallowMount(CorpusTableVue, {
      mocks: {
        $route: {
          params: {
            corpusId: null,
          },
        },
        $t: (key: string) => key,
      },
      stubs: {
        vMain: {
          template: '<span />',
        },
      },
    });
    expect(wrapper).toBeTruthy();
  });
});
