import type { Piece } from '@/data';
import { getCorpus, getIndexPieceAlt, sortCorpus } from '@/components/utils/nav-tools';
import { shallowMount } from '@vue/test-utils';
import TitleManager from '../../../src/components/navigation/TitleManager.vue';
import PieceParser from '../../../src/formats/PieceParser';
import type { Corpus } from '../../../src/client/corpus.js';

describe('TitleManager.vue', () => {
  function makePiece(path: string, title: string, isLocale = false) {
    let id = path;
    if (path.includes('/')) { id = path.split('/').pop() as string; }
    return PieceParser.fromFormat(path, isLocale, {
      id,
      title,
      opus: {},
      sources: {},
    });
  }
  const corpus: Corpus = { pieces: [] };
  const piece1: Piece = makePiece('corpus/corpus-1', 'AAA');
  const piece2: Piece = makePiece('corpus/corpus-2', 'BBB');
  const piece3: Piece = makePiece('corpus/corpus-3', 'CCC');
  const noCorpus1: Piece = makePiece('corpus-1', 'AAA');
  const noCorpus2: Piece = makePiece('corpus-2', 'BBB');
  const noCorpus3: Piece = makePiece('corpus-3', 'CCC');
  const corpusDuplicates = corpus;
  corpusDuplicates.pieces.push(...corpus.pieces);
  const subCorpus = {
    pieces: [
      makePiece('corpus/sub-1/sub-1-corpus-1', 'sub1AAA'),
      makePiece('corpus/sub-1/sub-1-corpus-2', 'sub1BBB'),
      makePiece('corpus/sub-1/sub-1-corpus-3', 'sub1CCC'),
      makePiece('corpus/sub-2/sub-2-corpus-1', 'sub2AAA'),
      makePiece('corpus/sub-2/sub-2-corpus-2', 'sub2BBB'),
      makePiece('corpus/sub-2/sub-2-corpus-3', 'sub2CCC'),
      noCorpus1,
      noCorpus2,
      noCorpus3,
    ],
  };
  const subCorpusDuplicates = subCorpus;
  subCorpusDuplicates.pieces.push(...subCorpus.pieces);
  const subCorpusIdsTitles = {
    pieces: [
      makePiece('corpus/sub-1/corpus-1', 'AAA'),
      makePiece('corpus/sub-1/corpus-2', 'BBB'),
      makePiece('corpus/sub-1/corpus-3', 'CCC'),
      makePiece('corpus/sub-2/corpus-1', 'AAA'),
      makePiece('corpus/sub-2/corpus-2', 'BBB'),
      makePiece('corpus/sub-2/corpus-3', 'CCC'),
      noCorpus1,
      noCorpus2,
      noCorpus3,
    ],
  };
  const soloCorpus = {
    pieces: [
      piece1,
      noCorpus1,
    ],
  };
  const soloCorpusDuplicates = soloCorpus;
  soloCorpusDuplicates.pieces.push(...soloCorpus.pieces);
  const soloSubCorpus = {
    pieces: [
      makePiece('corpus/sub-1/sub-1-corpus-1', 'sub1AAA'),
      makePiece('corpus/sub-2/sub-2-corpus-1', 'sub2AAA'),
      noCorpus1,
    ],
  };
  const soloSubCorpusDuplicates = soloSubCorpus;
  soloSubCorpusDuplicates.pieces.push(...soloSubCorpus.pieces);
  const soloSubCorpusIdsTitles = {
    pieces: [
      makePiece('corpus/sub-1/corpus-1', 'AAA'),
      makePiece('corpus/sub-2/corpus-1', 'AAA'),
      noCorpus1,
    ],
  };

  beforeEach(() => {
    corpus.pieces = [
      piece1,
      piece2,
      piece3,
      noCorpus1,
      noCorpus2,
      noCorpus3,
    ];
  });

  // Since all tests have the same structure, let's make a function
  const test = async (d: {
    title: string;
    corpus: Corpus;
    prevPiece: Piece | null;
    currentPiece: Piece;
    nextPiece: Piece | null;
  }) => {
    const theCorpus = sortCorpus(await getCorpus(d.currentPiece, d.corpus));
    const index = getIndexPieceAlt(d.currentPiece, theCorpus);

    const wrapper = shallowMount(TitleManager, {
      propsData: {
        indexCurrentPiece: index,
        corpus: theCorpus,
      },
      mocks: {
        $i18n: {
          locale: 'en',
        },
        $store: {
          dispatch: (..._: unknown[]) => {},
          state: {
            indexCurrentPiece: index,
            corpus: theCorpus,
          },
        },
      },
    });

    expect(wrapper.vm.$data.prev).toEqual(d.prevPiece);
    expect(wrapper.vm.$data.piece).toEqual(d.currentPiece);
    expect(wrapper.vm.$data.next).toEqual(d.nextPiece);
  };

  // We will push all the informations needed for each test
  // and iterate on it
  const testBase: {
    title: string;
    corpus: Corpus;
    prevPiece: Piece | null;
    currentPiece: Piece;
    nextPiece: Piece | null;
  }[] = [];

  // We fill the datalist
  // For previous and next pieces
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of a corpus without problems',
    corpus,
    prevPiece: piece1,
    currentPiece: piece2,
    nextPiece: piece3,
  });
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of a corpus with duplicates',
    corpus: corpusDuplicates,
    prevPiece: piece1,
    currentPiece: piece2,
    nextPiece: piece3,
  });
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of a sub-corpus without problems',
    corpus: subCorpus,
    prevPiece: subCorpus.pieces[0],
    currentPiece: subCorpus.pieces[1],
    nextPiece: subCorpus.pieces[2],
  });
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of a sub-corpus with duplicates',
    corpus: subCorpusDuplicates,
    prevPiece: subCorpusDuplicates.pieces[0],
    currentPiece: subCorpusDuplicates.pieces[1],
    nextPiece: subCorpusDuplicates.pieces[2],
  });
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of a sub-corpus with same ids and titles as other sub-corpus',
    corpus: subCorpusIdsTitles,
    prevPiece: subCorpusIdsTitles.pieces[0],
    currentPiece: subCorpusIdsTitles.pieces[1],
    nextPiece: subCorpusIdsTitles.pieces[2],
  });
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of pieces not in a corpus and without problems',
    corpus,
    prevPiece: noCorpus1,
    currentPiece: noCorpus2,
    nextPiece: noCorpus3,
  });
  testBase.push({
    title: 'should get the previous and next pieces : in the middle of pieces not in a corpus and with duplicates',
    corpus: corpusDuplicates,
    prevPiece: noCorpus1,
    currentPiece: noCorpus2,
    nextPiece: noCorpus3,
  });

  // for previous piece
  testBase.push({
    title: 'should get the previous piece : at the end of a corpus without problems',
    corpus,
    prevPiece: piece2,
    currentPiece: piece3,
    nextPiece: null,
  });
  testBase.push({
    title: 'should get the previous piece : at the end of a corpus with duplicates',
    corpus: corpusDuplicates,
    prevPiece: piece2,
    currentPiece: piece3,
    nextPiece: null,
  });
  testBase.push({
    title: 'should get the previous piece : at the end of a sub-corpus without problems',
    corpus: subCorpus,
    prevPiece: subCorpus.pieces[1],
    currentPiece: subCorpus.pieces[2],
    nextPiece: null,
  });
  testBase.push({
    title: 'should get the previous piece : at the end of a sub-corpus with duplicates',
    corpus: subCorpusDuplicates,
    prevPiece: subCorpusDuplicates.pieces[1],
    currentPiece: subCorpusDuplicates.pieces[2],
    nextPiece: null,
  });
  testBase.push({
    title: 'should get the previous piece : at the end of a sub-corpus with same ids and titles as other sub-corpus',
    corpus: subCorpusIdsTitles,
    prevPiece: subCorpusIdsTitles.pieces[1],
    currentPiece: subCorpusIdsTitles.pieces[2],
    nextPiece: null,
  });
  testBase.push({
    title: 'should get the previous piece : at the end of pieces not in a corpus and without problems',
    corpus,
    prevPiece: noCorpus2,
    currentPiece: noCorpus3,
    nextPiece: null,
  });
  testBase.push({
    title: 'should get the previous piece : at the end of pieces not in a corpus and with duplicates',
    corpus: corpusDuplicates,
    prevPiece: noCorpus2,
    currentPiece: noCorpus3,
    nextPiece: null,
  });

  // for next piece
  testBase.push({
    title: 'should get the next piece : at the beginning of a corpus without problems',
    corpus,
    prevPiece: null,
    currentPiece: piece1,
    nextPiece: piece2,
  });
  testBase.push({
    title: 'should get the next piece : at the beginning of a corpus with duplicates',
    corpus: corpusDuplicates,
    prevPiece: null,
    currentPiece: piece1,
    nextPiece: piece2,
  });
  testBase.push({
    title: 'should get the next piece : at the beginning of a sub-corpus without problems',
    corpus: subCorpus,
    prevPiece: null,
    currentPiece: subCorpus.pieces[0],
    nextPiece: subCorpus.pieces[1],
  });
  testBase.push({
    title: 'should get the next piece : at the beginning of a sub-corpus with duplicates',
    corpus: subCorpusDuplicates,
    prevPiece: null,
    currentPiece: subCorpusDuplicates.pieces[0],
    nextPiece: subCorpusDuplicates.pieces[1],
  });
  testBase.push({
    title: 'should get the next piece : at the beginning of a sub-corpus with same ids and titles as other sub-corpus',
    corpus: subCorpusIdsTitles,
    prevPiece: null,
    currentPiece: subCorpusIdsTitles.pieces[0],
    nextPiece: subCorpusIdsTitles.pieces[1],
  });
  testBase.push({
    title: 'should get the next piece : at the beginning of pieces not in a corpus and without problems',
    corpus,
    prevPiece: null,
    currentPiece: noCorpus1,
    nextPiece: noCorpus2,
  });
  testBase.push({
    title: 'should get the next piece : at the beginning of pieces not in a corpus and with duplicates',
    corpus: corpusDuplicates,
    prevPiece: null,
    currentPiece: noCorpus1,
    nextPiece: noCorpus2,
  });

  // for no piece
  testBase.push({
    title: 'should get no piece : in a corpus with only the current piece',
    corpus: soloCorpus,
    prevPiece: null,
    currentPiece: piece1,
    nextPiece: null,
  });
  testBase.push({
    title: 'should get no piece : in a corpus with only the current piece and its duplicates',
    corpus: soloCorpusDuplicates,
    prevPiece: null,
    currentPiece: piece1,
    nextPiece: null,
  });
  testBase.push({
    title: 'should get no piece : in a sub-corpus with only the current piece',
    corpus: soloSubCorpus,
    prevPiece: null,
    currentPiece: soloSubCorpus.pieces[0],
    nextPiece: null,
  });
  testBase.push({
    title: 'should get no piece : in a sub-corpus with only the current piece and its duplicates',
    corpus: soloSubCorpusDuplicates,
    prevPiece: null,
    currentPiece: soloSubCorpusDuplicates.pieces[0],
    nextPiece: null,
  });
  testBase.push({
    title: 'should get no piece : in a sub-corpus with only the current piece with same id and title as other sub-corpus',
    corpus: soloSubCorpusIdsTitles,
    prevPiece: null,
    currentPiece: soloSubCorpusIdsTitles.pieces[0],
    nextPiece: null,
  });
  testBase.push({
    title: 'should get no piece : with only the current piece in no corpus',
    corpus: soloCorpus,
    prevPiece: null,
    currentPiece: noCorpus1,
    nextPiece: null,
  });
  testBase.push({
    title: 'should get no piece : with only the current piece and its duplicates in no corpus',
    corpus: soloCorpusDuplicates,
    prevPiece: null,
    currentPiece: noCorpus1,
    nextPiece: null,
  });

  // We iterate
  testBase.forEach((data) => it(data.title, async () => {
    await test(data);
  }));
});
