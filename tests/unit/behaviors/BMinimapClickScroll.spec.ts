import BMinimapClickScroll from '@/components/behaviors/scroll/BMinimapClickScroll.vue';
import 'vuetify';
import type { Rect } from '@/data';
import type { BehaviorContext } from './utils';
import {
  getFakePixels,
  initBehaviorContext,
} from './utils';

describe('BMinimapClickScroll.vue', () => {
  // The shared variables for testing
  const scrollTo = jest.fn();
  let behaviorContext: BehaviorContext;

  // The init sequence
  beforeEach(() => {
    behaviorContext = initBehaviorContext(BMinimapClickScroll, {
      scrollTo,
      pixelUnit: getFakePixels(),
      viewRect: {
        x: 0, y: 0, width: 10, height: 10,
      } as Rect,
    });
  });

  // The reset sequence
  afterEach(() => {
    scrollTo.mockClear();
  });

  it('check that scroll starts uninit', async () => {
    const { behavior } = behaviorContext;
    expect(behavior.vm.state).toBe(null);
  });

  it('check that drag inits on viewrect drag', async () => {
    const { behavior, testEmit } = behaviorContext;
    await testEmit('all-mousedown', { x: 0, y: 0 });
    expect(behavior.vm.state).toBeTruthy();
  });

  it('check that drag doesn\'t init on non-viewrect drag', async () => {
    const { behavior, testEmit } = behaviorContext;
    await testEmit('all-mousedown', { x: 100, y: 0 });
    expect(behavior.vm.state).toBe(null);
  });

  it('check that drag resets', async () => {
    const { behavior, testEmit } = behaviorContext;
    await testEmit('all-mousedown', { x: 0, y: 0 });
    await testEmit('document-mousemove', { x: 100, y: 0 });
    await testEmit('document-mouseup', { x: 0, y: 0 });
    expect(behavior.vm.state).toBe(null);
  });

  it('check smooth scrollTo on non-viewrect mousedown', async () => {
    const { testEmit } = behaviorContext;
    await testEmit('all-mousedown', { x: 100, y: 0 });
    // scroll value is click.pos + viewRect.width / 2
    expect(scrollTo).toHaveBeenLastCalledWith(100 - 10 / 2, true);
  });

  it('check hard scrollTo on non-viewrect ctrl-mousedown', async () => {
    const { testEmit } = behaviorContext;
    await testEmit('all-ctrl-mousedown', { x: 100, y: 0 });
    // scroll value is click.pos + viewRect.width / 2
    expect(scrollTo).toHaveBeenLastCalledWith(100 - 10 / 2, false);
  });

  it('check that drag works', async () => {
    const { testEmit } = behaviorContext;
    await testEmit('all-mousedown', { x: 0, y: 0 });
    await testEmit('document-mousemove', { x: 100, y: 0 });
    await testEmit('document-mouseup', { x: 0, y: 0 });
    expect(scrollTo).toHaveBeenLastCalledWith(100, false);
  });

  it('check that drag works with offset', async () => {
    const { testEmit } = behaviorContext;
    await testEmit('all-mousedown', { x: 10, y: 0 });
    await testEmit('document-mousemove', { x: 110, y: 0 });
    await testEmit('document-mouseup', { x: 0, y: 0 });
    expect(scrollTo).toHaveBeenLastCalledWith(100, false);
  });
});
