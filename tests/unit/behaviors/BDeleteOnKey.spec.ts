import BDeleteOnKey from '@/components/behaviors/shared/BDeleteOnKey.vue';
import 'vuetify';
import { initBehaviorContext } from './utils';

describe('BDeleteOnKey.vue', () => {
  it('check deleting labels on delete pressed', async () => {
    const {
      testEmit, canvasEmit, actions, resetActions,
    } = initBehaviorContext(BDeleteOnKey, {});

    testEmit('document-keydown', { key: 'Enter' });
    expect(actions.deleteId).not.toHaveBeenCalled();

    testEmit('document-keydown', { key: 'Delete' });
    expect(actions.deleteId).not.toHaveBeenCalled();

    // Mocks the selection
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    actions.getSelected = jest.fn(() => ({ id: 0 })) as any;
    await testEmit('document-keydown', { key: 'Delete' });
    expect(actions.deleteSelected).toHaveBeenLastCalledWith(true);

    resetActions();

    // Mocks the next label
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    actions.deleteSelected = jest.fn(() => ({ id: 2 })) as any;

    await testEmit('document-keydown', { key: 'Delete' });
    expect(actions.deleteSelected).toHaveBeenLastCalledWith(true);
    expect(canvasEmit).toHaveBeenLastCalledWith('scroll-to-selection');

    // Mocks the prev label
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    actions.deleteSelected = jest.fn(() => ({ id: 0 })) as any;
    await testEmit('document-keydown', { key: 'Delete' });
    expect(actions.deleteSelected).toHaveBeenLastCalledWith(true);
    expect(canvasEmit).toHaveBeenLastCalledWith('scroll-to-selection');
  });
});
