import BCreateOnCanvasDrag from '@/components/behaviors/analysis/BCreateOnCanvasDrag.vue';
import 'vuetify';
import type { Point } from '@/data';
import {
  getFakePixels,
  getFakeStaff,
  MOCK_LABEL_ID,
  MOCK_STAFF_ID,
  initBehaviorContext,
} from './utils';

describe('BCreateOnCanvasDrag.vue', () => {
  it('check starting label creation on canvas drag', async () => {
    // Get staff
    const staff = getFakeStaff();

    // Get behavior fake context
    const {
      actions,
      canvasEmit,
      selfEmit,
      testEmit,
      resetActions,
    } = initBehaviorContext(BCreateOnCanvasDrag, {
      pixelUnit: getFakePixels(),
      snap: true,
    });

    expect(actions.createLabel).not.toHaveBeenCalled();
    expect(actions.updateLabel).not.toHaveBeenCalled();

    await testEmit('all-mousemove', { x: 0, y: 0 } as Point);
    await testEmit('all-mousemove', { x: 20, y: 0 } as Point);
    await testEmit('staff-mousedown', { x: 0, y: 50 } as Point, staff);

    expect(selfEmit).not.toHaveBeenCalled();
    expect(canvasEmit).not.toHaveBeenCalled();
    expect(actions.createLabel).not.toHaveBeenCalled();
    expect(actions.updateLabel).not.toHaveBeenCalled();

    await testEmit('all-mousemove', { x: 50, y: 30 });

    expect(canvasEmit).toHaveBeenLastCalledWith('start-resize', MOCK_LABEL_ID, staff, 0, 0);
    expect(actions.select).toHaveBeenLastCalledWith(MOCK_LABEL_ID);

    expect(actions.createLabel).toHaveBeenLastCalledWith();
    expect(actions.createLabel).toHaveBeenCalledTimes(1);

    expect(actions.updateLabel).toHaveBeenNthCalledWith(1, MOCK_LABEL_ID, 'onset', 0);
    expect(actions.updateLabel).toHaveBeenNthCalledWith(2, MOCK_LABEL_ID, 'staffId', MOCK_STAFF_ID);
    expect(actions.updateLabel).toHaveBeenCalledTimes(2);

    resetActions();
    expect(actions.createLabel).not.toHaveBeenCalled();
    canvasEmit.mockClear();
    selfEmit.mockClear();
    await testEmit('all-mousemove', { x: 100, y: 30 });

    expect(canvasEmit).not.toHaveBeenCalled();
    expect(selfEmit).not.toHaveBeenCalled();
    expect(actions.createLabel).not.toHaveBeenCalled();
    expect(actions.updateLabel).not.toHaveBeenCalled();
  });
});
